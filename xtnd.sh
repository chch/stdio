#!/bin/bash

# EXTEND HTML LAYOUT                                                          #
# --------------------------------------------------------------------------- #
# Copyright (C) 2021 Christoph Haag                                           #
# --------------------------------------------------------------------------- #

# This is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# 
# The software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License below for more details.
# 
# -> http://www.gnu.org/licenses/gpl.txt

# --------------------------------------------------------------------------- #
# INCLUDE FUNCTIONS
# --------------------------------------------------------------------------- #
  source lib/sh/select.functions
# --------------------------------------------------------------------------- #
# HANDLE INPUT ARGUMENTS
# --------------------------------------------------------------------------- #
  HTMLOLD=`echo $* | sed 's/ /\n/g' | grep "\.html$"`
  if [ -z $HTMLOLD ] || [ ! -f $HTMLOLD ] 
  then echo;echo "A HTML FILE PLEASE!"
            echo "e.g. $0 _/20161110212041.html";echo
     exit 0;
  fi
  INPUTLISTS=`echo $* | sed 's/ /\n/g' | grep -v "^-" | grep "\.list$"`
  for THIS in $INPUTLISTS
   do L=`echo $THIS | sed 's/^[0-9]\+x//'` #
      N=`echo $THIS                      | #
         sed 's/^\([0-9]\+\)\(.*\)/\1/'  | #
         egrep '^[0-9]+$'`                 #
      if [ -f "$L" ]
       then if [ "$N" != "" ]
             then L=`seq $N | sed "s|^.*$|$L|"`
            fi
            LALL=`echo $LALL $L`
      fi
  done; INPUTLISTS="$LALL"
  EXCEPTIONS=`echo $* | sed 's/-except/\n&/g' | sed 's/ /\n/g' | #
              grep "^-except" | cut -d "=" -f 2-`
  if [ "$EXCEPTIONS" == "" ]
  then   EXCEPTIONS=`grep "-except" $HTMLOLD | sed 's/ /\n/g' | #
                     grep "^-except" | cut -d "=" -f 2-`        #
  fi
 #---
  if [ `echo $* | sed 's/ /\n/g'    | #
        grep -- "^--neu"            | #
        wc -l` -gt 0 ];then NEU="YES"
                       else NEU="NO";fi
  if [ `echo $* | sed 's/ /\n/g'    | #
        grep -- "^--shuf"           | #
        wc -l` -gt 0 ];then SHUF="shuf"
                       else SHUF="tee";fi
  if [ `echo $* | sed 's/ /\n/g'    | #
        grep -- "^--allow-rotation" | #
        wc -l` -gt 0 ];then R="YES"
                       else R="NO";fi
 #---
  FLAGS=`echo $* | sed 's/--/-/g'`
# --------------------------------------------------------------------------- #
# STATIC DEFINITIONS
# --------------------------------------------------------------------------- #
  OUTDIR="_";LSDIR="E";SRCDIR="src/ioio";TMP="/tmp/SZDIOX${RANDOM}";
 #----
   ELEMENTLIST="${TMP}.all";touch $ELEMENTLIST
   if [ `ls $INPUTLISTS | wc -l` -ge 1 ] &&
      [ `echo $INPUTLISTS | wc -c` -ge 2 ]
    then cat $INPUTLISTS              > $ELEMENTLIST
   fi
  IORRAYOLD=`grep 'src="[^"]*.ios"' $HTMLOLD | #
             sed 's/\(.*src="\)\([^"]*.ios\)\(".*\)/\2/'`
  IORRAYOLD=`echo $IORRAYOLD | sed 's,^../,,'`
     IORRAY=${OUTDIR}/`date +%y%m%d%H%M%S`.ios
    HTMLNEW=${OUTDIR}/xtnd.html
 #----
  for XCPTN in `echo $EXCEPTIONS | sed 's/;/\n/g'`
    do  L=`echo $XCPTN | cut -d ":" -f 2`
        echo $XCPTN | sed "s|,|:$L\n|g" >> ${TMP}.tmp
  done; touch ${TMP}.tmp # MAKE SURE FILE EXISTS ANYWAY

  cat ${TMP}.tmp                    | # USELESS USE OF CAT
  sed 's/\b[RC]\+[0-9]\+/\n&/g'     | #
  sed 's/:$//' | sed '/^[ ]*$/d'    | #
  awk 'BEGIN { FS = ":" } ; \
      { print length($1) ":" $0; }' | #
  sort -nr | cut -d ":" -f 2-       | #
  tee  > ${TMP}.exceptions            # WRITE TO FILE
 #----
# --------------------------------------------------------------------------- #
# ADD ELEMENTS TO LIST
# --------------------------------------------------------------------------- #
  cat $ELEMENTLIST > ${TMP}.1.list
  ELEMENTLIST=${TMP}.kombi.list
  cat ${TMP}.1.list > $ELEMENTLIST
 #---
  if [ "$NEU" == "YES" ];then
 #---
  KEEPELEMENTS=`cat $ELEMENTLIST          | #
                cut -d "_" -f 1 | sort -u | #
                sed ':a;N;$!ba;s/\n/|/g'`   #
  HTMLELEMENTS=`grep "url(" $HTMLOLD      | #
                sed 's/.*:url(//'         | #
                cut -d ">" -f 1           | #
                sed 's/)" class="/ /'     | #
                sed 's/[rc][0-9]\+//g'    | #
                sed 's/k//'               | #
                sed 's/ /:/'              | #
                sed 's/"//'               | #
                sed 's/a/ROTO/'           | #
                sed '/flip/s/ROTO/FLIP/'  | #
                sed '/flop/s/ROTO/FLOP/'  | #
                sed 's/[ ]*fl[io]p[ ]*//' | #
                sed 's/ //g'              | #
                cut -d ":" -f 1           | #
                sort -u                   | #
                tee -a $ELEMENTLIST       | #
                cut -d "_" -f 1           | #
                egrep -v "$KEEPELEMENTS"  | #
                sed ':a;N;$!ba;s/\n/|/g'  | #
                sed 's/|$//'`               #
  else          cat $IORRAYOLD  | #
                cut -d "'" -f 2 | #
                cut -d ":" -f 1 | #  
                grep -v '^//'     >> $ELEMENTLIST
  HTMLELEMENTS="X"
 #---
  fi;cp $ELEMENTLIST ${ELEMENTLIST}.standard
# --------------------------------------------------------------------------- #
# EXTEND ARRAY
# --------------------------------------------------------------------------- #
 (IFS=$'\n'
  for ELEMENT in `grep "^R" $IORRAYOLD | #
                  cut -d ":" -f 1      | #
                  sed "s/ = \['/ /"`     #
   do
    # ----------------------------------------------------------------- # 
      ID=`echo $ELEMENT  | cut -d " " -f 1`
      XP=`echo $ID | cut -c 5-6`;YP=`echo $ID | cut -c 2-3`
    # ----------------------------------------------------------------- #
    # GET FIRST ELEMENT FROM OLD HTML (BUGGY?)
    # ----------------------------------------------------------------- #
      E1=`grep "$ID" $HTMLOLD       | #
          grep "url("               | #
          sed 's/.*:url(//'         | #
          cut -d ">" -f 1           | #
          sed 's/)" class="/ /'     | #
          sed 's/[rc][0-9]\+//g'    | #
          sed 's/k//'               | #
          sed 's/ /:/'              | #
          sed 's/"//'               | #
          sed 's/a/ROTO/'           | #
          sed '/flip/s/ROTO/FLIP/'  | #
          sed '/flop/s/ROTO/FLOP/'  | #
          sed 's/[ ]*fl[io]p[ ]*//' | #
          sed 's/ //g'`               #
    # ----------------------------------------------------------------- #
      A=`echo $E1 | cut -d ":" -f 2`;
      function c(){ echo $E1 | cut -d "_" -f 2- | cut -d "_" -f $1; }
    # ----------------------------------------------------------------- #
      if [ "$A" = ROTO000 ];then F1=`c 1`;F2=`c 2`;F3=`c 3`;F4=`c 4`;fi
      if [ "$A" = ROTO090 ];then F1=`c 4`;F2=`c 1`;F3=`c 2`;F4=`c 3`;fi
      if [ "$A" = ROTO180 ];then F1=`c 3`;F2=`c 4`;F3=`c 1`;F4=`c 2`;fi
      if [ "$A" = ROTO270 ];then F1=`c 2`;F2=`c 3`;F3=`c 4`;F4=`c 1`;fi
      if [ "$A" = FLIP000 ];then F1=`c 1`;F2=`c 4`;F3=`c 3`;F4=`c 2`;fi
      if [ "$A" = FLIP090 ];then F1=`c 4`;F2=`c 3`;F3=`c 2`;F4=`c 1`;fi
      if [ "$A" = FLIP180 ];then F1=`c 3`;F2=`c 2`;F3=`c 1`;F4=`c 4`;fi
      if [ "$A" = FLIP270 ];then F1=`c 2`;F2=`c 1`;F3=`c 4`;F4=`c 3`;fi
      if [ "$A" = FLOP000 ];then F1=`c 3`;F2=`c 2`;F3=`c 1`;F4=`c 4`;fi
      if [ "$A" = FLOP090 ];then F1=`c 2`;F2=`c 1`;F3=`c 4`;F4=`c 3`;fi
      if [ "$A" = FLOP180 ];then F1=`c 1`;F2=`c 4`;F3=`c 3`;F4=`c 2`;fi
      if [ "$A" = FLOP270 ];then F1=`c 4`;F2=`c 3`;F3=`c 2`;F4=`c 1`;fi
    # ----------------------------------------------------------------- #
    # CHECK IF EXCEPTIONAL
    # ----------------------------------------------------------------- #
      XCPTN=`egrep "^R${YP}C${XP}:|^R${YP}:|^C${XP}:" \
             ${TMP}.exceptions | head -n 1`
      if [ "$XCPTN" != "" ]
      then XLST=`echo $XCPTN | cut -d ":" -f 2`
           if [ `ls $XLST | #
                 wc -l` -gt 0 ]
           then echo -e "       \e[104m\e[97m USE EXCEPTION \e[0m"
                LID=`echo $(($RANDOM + 50000 * 4)) | #
                     rev | cut -c 1-6`
                cat $XLST | #
                sed "s/^/${LID}|/" > $ELEMENTLIST
                RNDX="..";RNDY=".."
            fi
      else      cp ${ELEMENTLIST}.standard $ELEMENTLIST
      fi
    # ----------------------------------------------------------------- #
      selectElement
    # ----------------------------------------------------------------- # 
      SELECTMORE=`echo $SELECTMORE        | # 
                  sed 's/ /\n/g'          | #
                  egrep -v "$HTMLELEMENTS"` #
      if [ "$SELECTMORE" == "" ]
      then FILTER="tee";else FILTER="egrep -v \"$HTMLELEMENTS\""
      fi
    # ----------------------------------------------------------------- # 
    # WRITE ARRAY
    # ----------------------------------------------------------------- # 
      NL=N${RANDOM}L
      echo $E1 $SELECTMORE            | # SHOW RESULTS
      sed "s/ /\n/g"                  | # PRINT AS LINES
      eval $FILTER                    | # FILTER RESULTS 
       grep -n ""                     | # ADD LINENUMBERS
       sort -u -t ":" -k 2,3 | # SORT/UNIQ QUA FIELD 2+3 (=NAME+TRNSFRM)
       sort -n               | # SORT QUA LINENUMBERS (RESHUFFLE) ...
       cut -d ":" -f 2-      | # ... AND REMOVE LINENUMBERS
      eval $SHUF                      | # SHUFFLE (AS SET)
      sed "s,\(:\)\([0-9]*\)$,\10\2," | # ZEROPAD ROTATION
      sed ":a;N;\$!ba;s/\n/$NL/g"     | # PUT ON SEPARATE LINES
      sed "s/^/R${YP}C${XP} = ['/"    | # START ARRAY
      sed "s/$/'];/"                  | # CLOSE ARRAY
      sed "s/$NL/',\n          '/g"   | # INDENT
      tee  >> $IORRAY                   # APPEND TO ARRAY
    # ----------------------------------------------------------------- # 
      echo "$ID "`grep R${YP}C${XP} $IORRAY | cut -d "'" -f 2`
    # ----------------------------------------------------------------- # 
  done;)
# --------------------------------------------------------------------------- #
# CREATE NEW HTML
# --------------------------------------------------------------------------- #
  echo "<head>"                                                   >  $HTMLNEW
  echo "<!-- $0 $FLAGS ("`date +%d.%m.%Y" "%H:%M:%S`") -->"       >> $HTMLNEW   
  cat $HTMLOLD                 | #
  sed '/^<head>$/d'            | #
  sed "s,$IORRAYOLD,$IORRAY,g" | #
  grep -v  '^<!--.*-->$'       | #
  egrep -v '^<!--|-->$'        | #
  tee                                                             >> $HTMLNEW
 (IFS=$'\n'
  for ELEMENT in `grep "^R" $IORRAY  | #
                  cut -d ":" -f 1    | #
                  sed "s/ = \['/ /"`   #
   do
      ID=`echo $ELEMENT  | cut -d " " -f 1`
      XP=`echo $ID | cut -c 5-6`;YP=`echo $ID | cut -c 2-3`
      CLASS="class=\"r${YP} c${XP}"

      ISSOLO=`grep "$ID" $IORRAY | grep "];$" | wc -l`
      HASCYCLISTE=`grep "$ID" $HTMLNEW | grep cycliste | wc -l`

      ELEMENT=`grep "R${YP}C${XP}" $IORRAY | head -n 1                          | #
               sed "s,^.* = \[',<div id=\"$ID\" style=\"background-image:url(," | #
               sed "s,\([0-9]\{3\}\)'\]*;*\,*$,)\" CXLXAXSXS a\1\"></div>,"     | #
               sed "s/:\([FLIRTOP]\{4\}\))\" CXLXAXSXS/)\" $CLASS k \1/"        | #
               sed 's/ ROTO / /'      | #
               sed 's/ FLIP / flip /' | #
               sed 's/ FLOP / flop /' | #
               sed 's/"/\\\"/g' | sed 's/\//\\\\\//g'`
      sed -i "/id=\"$ID\"/s/^.*$/$ELEMENT/" $HTMLNEW

      if [ $ISSOLO == "0" ] &&
         [ $HASCYCLISTE == "0" ]
      then #echo "$ID IS NOT SOLO AND HAS NO CYCLISTE"
           OLDLMNT="^<div.*$CLASS.*#R${YP}C${XP}.*>$"
           CYCLISTE=`echo "<div $CLASS b cycliste\"> \
                     <a href=\\\\\"#R${YP}C${XP}\\\\\"> \
                     </a></div>"             | #
                     sed ':a;N;$!ba;s/\n//g' | #
                     sed 's/>[ ]*</></g'`
           sed -i "/$OLDLMNT/d" $HTMLNEW
           sed -i "s,^.*id=\"$ID\".*$,&\n$CYCLISTE," $HTMLNEW
      elif [ $ISSOLO == "1" ] &&
           [ $HASCYCLISTE == "1" ] 
      then   #echo "$ID IS SOLO AND HAS CYCLISTE"
             sed -i "/${ID}/s/[ ]*cycliste[ ]*//g" $HTMLNEW
      fi

  done;)
# --------------------------------------------------------------------------- #
# CLEAN UP
# --------------------------------------------------------------------------- #
  if [ `echo ${TMP} | wc -c` -ge 4 ] &&
     [ `ls ${TMP}*.* 2>/dev/null | wc -l` -gt 0 ]
  then  rm ${TMP}*.* ;fi

exit 0;
