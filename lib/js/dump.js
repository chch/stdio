// ------------------------------------------------------------------------- //
// http://stackoverflow.com/questions/3605214/
// -> javascript-add-leading-zeroes-to-date
// ------------------------------------------------------------------------- //
   var d;var iosName;
   function dateString(d){
       function pad(n){return n<10 ? '0'+n : n}
       return d.getFullYear()      + "" + 
              pad(d.getMonth()+1)  + "" +
              pad(d.getDate())     + "" +
              pad(d.getHours())    + "" +
              pad(d.getMinutes())  + "" +
              pad(d.getSeconds());
   }
// ------------------------------------------------------------------------- //
// SAVE HTML PAGE
// http://jsfiddle.net/k56eezxp/
// ------------------------------------------------------------------------- //
   window.onload=function(){

   var create = document.getElementById('create');
   
     (function () {
        var textFile = null,
        makeTextFile = function (text) {
          var data = new Blob([text], {type: 'text/plain'});
        // If we are replacing a previously generated file we need to
        // manually revoke the object URL to avoid memory leaks.
          if (textFile !== null) {
            window.URL.revokeObjectURL(textFile);
          } 
          textFile = window.URL.createObjectURL(data);
          return textFile;
        };
  
        create.addEventListener('click', function () {

          link = document.createElement('a');
          d = new Date();
          htmlName = dateString(d) + ".html"
          iosName = dateString(d).substring(2,15) + ".ios"

          link.setAttribute('download', htmlName);
          re = new RegExp(basepath + "(.{30})" + ".[gifsvg]{3}" ,"g");
          if (iosModified == false) { 
              link.href = makeTextFile($('html').clone().html()
                                                .replace(re,"$1"));
          }  else {
              link.href = makeTextFile($('html').clone().html()
                                                .replace(re,"$1")
                                                .replace(/[0-9]{12}\.ios/,iosName));
          }
          document.body.appendChild(link);
        // wait for the link to be added to the document
          window.requestAnimationFrame(function () {
            var event = new MouseEvent('click');
            link.dispatchEvent(event);
            document.body.removeChild(link);
          });
        }, false);

     })();
   }
// ------------------------------------------------------------------------- //
// DISPLAY AND WRITE IOS ARRAY (MINUS REMOVED ELEMENTS)
// http://simey.me/saving-loading-files-with-javascript/
// ------------------------------------------------------------------------- //
   var modIOS = {}; // https://stackoverflow.com/questions/7259728 
   $( document ).ready(function() {

    create.addEventListener('click', function () {

     if (iosModified == true) { 

        $('div.k').each(function() {
            eID  = $(this).attr('id');
            modIOS[eID] = {};
            var arrayLength = eval(eID).length;
            for (var i = 0; i < arrayLength; i++) {
                     modIOS[eID][i] = eval(eID)[i];
            }
        });

        saveIOS = JSON.stringify(modIOS);



        // create a link DOM fragment
        var $link = $("<a />");  
        var text = saveIOS.replace(/:{/g," = [")
                          .replace(/},/g,"]\n")
                          .replace(/"[0-9]+":/g,",")
                          .replace(/",,"/g,"',\n          '")
                          .replace(/"]/g,"'];")
                          .replace(/}}/g,"'];\n")
                          .replace(/,"/g,"'")
                          .replace(/"/g,'')
                          .replace(/{/g,'');
        // http://simey.me/saving-loading-files-with-javascript/
        $link
          .attr( "download", iosName )
        //.attr( "href", "data:application/octet-stream," + text )
          .attr( "href", "data:text/plain;charset=utf-8,"  
                          + encodeURIComponent(text) )
          .appendTo( "body" )
          .get(0)
          .click()
     }
    }, false);
   });
// ------------------------------------------------------------------------- //

