var deleteMode = false;
var basepath = "../src/ioio/";
var iosModified = false;

// ------------------------------------------------------------------------- //
// INIT: 
// ------------------------------------------------------------------------- //
  $( document ).ready(function() {
// ------------------------------------------------------------------------- //
  // ------------------------------------------------------------------- //
     $('div.k').each(function() {

            name  = $(this).attr('id');
        initName  = this.style.backgroundImage
                             .replace(/(url\(|\)|'|")/gi,'');
        initPath  = basepath + initName;
        initClass = $(this).attr('class');
        initHTML  = '<div id="' + name + 
                    '" style="background-image:url(' 
                    + initPath + ".gif" + 
                    ')" class="' + initClass + '"></div>';
        initSVG   = initPath + ".svg";
        document.getElementById(name).outerHTML = initHTML;
        document.getElementById(name + "SRC").href = initSVG;

     });

     $('#togglesrc').click(function(e) {
            $('.svgsrc').toggle();
            e.preventDefault();            
     });

     $('.cycliste').click(function(e) {
            eID = $(this).attr('class')
                         .replace(/b/,'')
                         .replace(/cycliste/,'')
                         .replace(/start/g,'')
                         .replace(/ /g,'')
                         .toUpperCase();
            cycle(eID,'r',$(this));
            e.preventDefault();
     });
// ------------------------------------------------------------------------ // 
  });
// ------------------------------------------------------------------------ // 
// KEYBOARD CONTROL
// ------------------------------------------------------------------------ // 
   $(document).ready(function(){
// ------------------------------------------------------------------------ // 
    window.addEventListener("keydown", function(e) {
    // backspace
    if([8].indexOf(e.keyCode) > -1) {
        e.preventDefault();
    }
   }, false);
// ------------------------------------------------------------------------- //
   $(window).bind('keydown',function(e) {
     keyCode = e.keyCode || e.which;
  // ----
     if ( keyCode == 8 && deleteMode == false) {
          $('body').addClass('delete');
          deleteMode = true;
     }
  // ----
   })
// ------------------------------------------------------------------------- //
   $(window).bind('keyup',function(e) {
     keyCode = e.keyCode || e.which;
  // ----
     if ( keyCode == 8 && deleteMode == true) {
          $('body').removeClass('delete');
          deleteMode = false
     }
  // ----
   })
// ------------------------------------------------------------------------- //
   })
// ------------------------------------------------------------------------- //
   // https://stackoverflow.com/questions/3715496/
   // -> binding-to-the-scroll-wheel-when-over-a-div

   function isEventSupported(eventName) {
       var el = document.createElement('div');
       eventName = 'on' + eventName;
       var isSupported = (eventName in el);
       if (!isSupported) {
           el.setAttribute(eventName, 'return;');
           isSupported = typeof el[eventName] == 'function';
       }
       el = null;
       return isSupported;
   }
// ------------------------------------------------------------------------- //   
   $(document).ready(function() {

     // Check which wheel event is supported.
     // Don't use both as it would fire each event 
     // in browsers where both events are supported.
     var wheelEvent = isEventSupported('mousewheel') ? 'mousewheel' : 'wheel';
 
     // Disable scroll wheel for whole window
     $(window).on(wheelEvent,function(e){e.preventDefault();});

     // Now bind the event to the desired element
     $('.cycliste').on(wheelEvent, function(e) {

         if (deleteMode != true ) {

           var oEvent = e.originalEvent,
               delta  = oEvent.deltaY || oEvent.wheelDelta;
   
           // deltaY for wheel event
           // wheelData for mousewheel event
   
           eID = $(this).attr('class')
                        .replace(/b/,'')
                        .replace(/cycliste/,'')
                        .replace(/start/,'')
                        .replace(/ /g,'')
                        .toUpperCase();

           if (delta > 0) { //console.log("SCROLL DOWN");
                            cycle(eID,'f',$(this));
           } else {         //console.log("SCROLL UP");
                            cycle(eID,'r',$(this));
           }
         }
        e.preventDefault();            
     });

        var modIOS = {};
     // SYNC HTML AND ARRAYS
       $('div.k').each(function() {
 
          eID = $(this).attr('id');
          startsrcpath   = $(this).attr('style')
                           .split("(")[1].split(")")[0];
          startsrcname   = startsrcpath
                           .split('/')
                           .reverse()[0]
                           .replace(/\.gif$/g,"");
          starttransform = $(this).attr('class')
                           .replace(/^........../,'')
                           .replace(/(fl[oi]p)( a)/,'$1')
                           .replace(/a/,'ROTO')
                           .toUpperCase();
          startsrc       = startsrcname + ":" + starttransform;
          startsrcnum    = jQuery.inArray(startsrc,eval(eID));
       
       // REMOVE DUPLICATES FROM ARRAY (BRUTFORCE)
          eval(eID).splice(startsrcnum,1);
 
       // ADD STARTER ELEMENT TO ARRAY
          eval(eID).push(startsrc);
      });

      $('div.cycliste').each(function() {
          $(this).addClass('start');
      });
   });
// ------------------------------------------------------------------------- //
   function cycle(ID,direction,cycliste) {
 
        name = ID;
     element = document.getElementById(ID);
  
     nowsrcpath   = $(element).attr('style').split("(")[1].split(")")[0];
     nowsrcname   = nowsrcpath.split('/').reverse()[0].replace(/\.gif$/g,"");
     nowclass     = $(element).attr('class'); 
     nowtransform = nowclass
                   .replace(/^........../,'')
                   .replace(/(fl[oi]p)( a)/,'$1')
                   .replace(/a/,'ROTO')
                   .toUpperCase();
     nowsrc       = nowsrcname + ":" + nowtransform;
     nowsrcnum    = jQuery.inArray(nowsrc,eval(name));
  
     if ( direction == 'f') { newsrcnum = nowsrcnum + 1; }
     if ( direction == 'r') { newsrcnum = nowsrcnum - 1; }
  
     if ( newsrcnum > eval(name).length - 1 ) { newsrcnum = 0; } 
     else if ( newsrcnum < 0 ) { newsrcnum = eval(name).length - 1; }
  
     if ( newsrcnum == eval(name).length - 1 ) { 
            cycliste.addClass('start');
     } else {
            cycliste.removeClass('start');
  
     }
  
     newsrc       = eval(name)[newsrcnum];
     newsrcname   = newsrc.split(":")[0];
     newtransform = newsrc.split(":")[1];
  
     newtransform =  newtransform
                    .replace(/^ROTO/g,'a')
                    .replace(/(FL[OI]P)([012])/g,'$1 a$2')
                    .toLowerCase();
     newclass     = name.replace(/...$/g,"");
     newclass     = newclass + " " + name.replace(/^.../g,"");
     newclass     = newclass.toLowerCase();
     newclass     = newclass + " k " + newtransform;
     newouterHTML = '<div id="' + name + 
                    '" style="background-image:url(' 
                    + basepath + newsrcname + ".gif" + 
                    ')" class="' + newclass + '"></div>';
     newsvg = newsrcname + ".svg";
  
     document.getElementById(name).outerHTML = newouterHTML;
     document.getElementById(name + "SRC").href = basepath + newsvg;

  // REMOVE
  // --------------------------------------------------------------------- //
     if ( deleteMode == true ) {
       if ( eval(name).length > 1 ) { eval(eID).splice(nowsrcnum,1); 
                                      iosModified = true;
       } else { cycliste.addClass('svgsrc');
                cycliste.removeClass('cycliste');
       }
     }
  // --------------------------------------------------------------------- //
   }
// ------------------------------------------------------------------------- //

