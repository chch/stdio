#!/bin/bash

  IOSSRC=`echo $* | sed 's/ /\n/g' | grep "\.ios" | head -n 1`
  NONINTERACTIVE=`echo $* | grep -- "-f" | wc -l` # QUIET

  HTMLDIR="../_";TMPDIR="/tmp"
# --------------------------------------------------------------------------- #
  HDREQUIRED=`echo $* | sed 's/ /\n/g' | egrep '^-[0-9]+$' | sed 's/-//g'`
# --
  SRCDIR=`echo $* | sed 's/ /\n/g' | egrep -- '^--srcdir='    | #
          cut -d '=' -f 2          | sed 's/\/$//'`             #
  if [ "$IOSSRC" == "" ] ||
     [ -d "$SRCDIR" ] &&
     [ `ls $SRCDIR/ | grep '\.svg$'            | #
        egrep '[A-F0-9]{,14}...[CUSI0WB_]{12}' | #
        wc -l` -gt 0 ]
  then SRCMODE="DIR";HDREQUIRED="";
  else SRCMODE="FILE";SRCDIR="../src/ioio"
  fi
# --
  if [ "$SRCMODE" == "FILE" ]
  then  if [ ! -f $IOSSRC ] || [ "$IOSSRC" == "" ]
        then echo "PLEASE PROVIDE INPUT FILE"
             echo "./cleanios.sh ../_/190204143930.ios"
             exit 1
        else SRCDIR="../src/ioio"
        fi
      # --
        if [ "$NONINTERACTIVE" != 1 ]
        then echo -e ":NN:\e[31mTHE FOLLOWING PROCESS 
                                WILL OVERWRITE $IOSSRC\e[0m" | #
             sed ':a;N;$!ba;s/\n//g' | tr -s ' '    | #
             sed 's/:NN:/\n/g' | sed 's/^[ ]*//'
             read -p "I KNOW WHAT I'M DOING? [y/n] " ANSWER
             if [ "$ANSWER" == "y" ]
             then echo;else echo -e "CIAO.";exit 0
             fi
        fi
  fi
# --
  if [ "$HDREQUIRED" != "" ]
  then MODUS="SIMILAR";             echo "CLEAR SIMILAR ELEMENTS"
  else MODUS="EQUAL";HDREQUIRED="0";echo "CLEAR EQUAL ELEMENTS"
  fi
# --
  HASHSTORE=`echo $* | sed 's/ /\n/g' | #
             egrep -- '^--store='     | #
             cut -d '=' -f 2`           #
  if [ "$HASHSTORE" == "" ] ||
     [ `dirname 2> /dev/null $HASHSTORE` == "" ]
  then  HASHSTORE="mkphash.txt"
  fi;   if [ ! -f $HASHSTORE ];then touch $HASHSTORE;fi
# --------------------------------------------------------------------------- #
  SP="/////-----\\\\\|||||" # https://unix.stackexchange.com/questions/225179
  TMP="$TMPDIR/iosclean";NLFOO="N${RANDOM}L"  
  if [ "$SRCMODE" == "DIR" ]
  then IOSSRC="${TMP}.ios"      #
       ls $SRCDIR             | #
       grep '\.svg$'          | #
       cut -d '.' -f 1        | #
       sed "s/^/'/"           | #
       sed "s/$/:ROTO000/"    | #
       sed "1s/^/R00C00 = [/" > $IOSSRC
  fi
# --------------------------------------------------------------------------- #
# COLLECT NECESSARY HASHES
# --------------------------------------------------------------------------- #
  for E in `sed "s/'/\n/g" $IOSSRC                    | #
            egrep '^[A-F0-9]{,12}.....[CUSI0WB_]{12}' | #
            cut -d ":" -f 1 | sort -u`                  #
   do  echo -en " COLLECT HASHES \r"
       printf "\b${SP:i++%${#SP}:1}"
       HASHDUMP="${HASHDUMP}|"`grep $E $HASHSTORE | sed 's/$/|/g'`
  done;HASHDUMP=`echo $HASHDUMP | #
                 sed 's/^|//'   | #
                 sed 's/|/\n/g' | #
                 sed 's/^ *//'  | #
                 sed '/^$/d'`     #
  echo -en "\r"
# --------------------------------------------------------------------------- #
  convert -size 1x1 xc:#ffffff xc:#000000 \
                    xc:#ff0000 xc:#00ff00 \
          -append ${TMP}.map.gif
# --------------------------------------------------------------------------- #
  function hamming() { HAMMING=0
                       for ((I=0;I<${#1};I++));
                       do  [ ${1:I:1} == ${2:I:1} ] || let "HAMMING++"
                       done;echo "$HAMMING"; }
# --------------------------------------------------------------------------- #
  function mkPHash() {

    CHECKNAME="$1"
     CHECKSRC="${SRCDIR}/${CHECKNAME}.svg"
      SRCHASH=`md5sum ${CHECKSRC} | cut -c 1-32`

    if [ `echo "$HASHDUMP" | grep "${CHECKNAME}:${SRCHASH}" | wc -l` -lt 1 ]
    then
         cat $CHECKSRC                                | # USELESS USE OF CAT
         sed 's/\(stroke-width:\)\([0-9\.]*\)/\125/g' | # INCREASE STROKE WIDTH
         sed "s/#ffffff/::WHITE::/gI"                 | # PROTECT WHITE
         sed "s/fill:#000000/::BLACKFILL::/gI"        | # PROTECT BLACK FILL
         sed "s/stroke:#000000/::BLACKSTROKE::/gI"    | # PROTECT BLACK STROKE
         sed "s/:#[0-9a-f]\{6\}/:#ff0000/gI"          | # EQUALIZE COLORS
         sed "s/::BLACKFILL::/fill:#00ff00/g"         | # UNPROTECT BLACK FILL
         sed "s/::BLACKSTROKE::/stroke:#000000/g"     | # UNPROTECT BLACK STROKE
         sed "s/::WHITE::/#ffffff/g"                  | # UNPROTECT WHITE
         tee                             > ${TMP}.svg   # WRITE TO FILE
     
         inkscape --export-png=${TMP}.png         \
                  --export-area=10:10:390:390     \
                  --export-background=ffffff      \
                  ${TMP}.svg > /dev/null 2>&1
         convert ${TMP}.png -resize 16x16 +dither \
                            -remap ${TMP}.map.gif \
                 ${TMP}.gif
     
         convert -rotate 000 ${TMP}.gif ${TMP}_ROTO000.gif
         convert -rotate 090 ${TMP}.gif ${TMP}_ROTO090.gif
         convert -rotate 180 ${TMP}.gif ${TMP}_ROTO180.gif
         convert -rotate 270 ${TMP}.gif ${TMP}_ROTO270.gif
     
       # 'BUG': FLIP/FLOP SWITCHED (DIFFERENT THAN IMAGEMAGICK)
       # http://www.imagemagick.org/Usage/warping/#flip
     
         convert -rotate 000 -flop ${TMP}.gif ${TMP}_FLIP000.gif
         convert -rotate 090 -flop ${TMP}.gif ${TMP}_FLIP090.gif
         convert -rotate 180 -flop ${TMP}.gif ${TMP}_FLIP180.gif
         convert -rotate 270 -flop ${TMP}.gif ${TMP}_FLIP270.gif
     
         convert -rotate 000 -flip ${TMP}.gif ${TMP}_FLOP000.gif
         convert -rotate 090 -flip ${TMP}.gif ${TMP}_FLOP090.gif
         convert -rotate 180 -flip ${TMP}.gif ${TMP}_FLOP180.gif
         convert -rotate 270 -flip ${TMP}.gif ${TMP}_FLOP270.gif
     
         PHASH=""
         for V in ROTO000 ROTO090 ROTO180 ROTO270 \
                  FLIP000 FLIP090 FLIP180 FLIP270 \
                  FLOP000 FLOP090 FLOP180 FLOP270
          do
             PHASH=$PHASH:`convert ${TMP}_${V}.gif txt: | # SHOW PIXEL VALUES
                           sed 's/^.*#ffffff.*$/0/gI'   | # REDUCE (WHITE)
                           sed 's/^.*#000000.*$/1/gI'   | # REDUCE (BLACK STROKE)
                           sed 's/^.*#ff0000.*$/2/gI'   | # REDUCE (BLACK FILL)
                           sed 's/^.*#00ff00.*$/3/gI'   | # REDUCE (REMAINS)
                           grep '^[0123]$'              | # TAKE REDUCED ONLY
                           sed -n '1~2p'                | # TAKE EVERY 2ND LINE
                           tr -d '\n'`                    # RM NEWLINES
         done
     
         HASHDUMP=`echo -e "$HASHDUMP\n${CHECKNAME}:${SRCHASH}${PHASH}"`
         echo ${CHECKNAME}:${SRCHASH}${PHASH} >> $HASHSTORE

   #else echo -en "HASH DONE ($CHECKNAME) \r"
    fi

  }
# --------------------------------------------------------------------------- #
  function showPHash() {

    SRCNAME="$1";SRCTRANSFORM="$2"
    SRCHASH=`md5sum ${SRCDIR}/${SRCNAME}.svg | cut -c 1-32`

     if   [ "$SRCTRANSFORM" == "ROTO000" ]
      then   VHASH=`echo "$HASHDUMP" | grep "^$SRCNAME" | #
                    grep "$SRCHASH"  | head -n 1        | #
                    cut -d ":" -f  3`                     #
     elif [ "$SRCTRANSFORM" == "ROTO090" ]
      then   VHASH=`echo "$HASHDUMP" | grep "^$SRCNAME" | #
                    grep "$SRCHASH"  | head -n 1        | #
                    cut -d ":" -f  4`                     #
     elif [ "$SRCTRANSFORM" == "ROTO180" ]
      then   VHASH=`echo "$HASHDUMP" | grep "^$SRCNAME" | #
                    grep "$SRCHASH"  | head -n 1        | #
                    cut -d ":" -f  5`                     #
     elif [ "$SRCTRANSFORM" == "ROTO270" ]
      then   VHASH=`echo "$HASHDUMP" | grep "^$SRCNAME" | #
                    grep "$SRCHASH"  | head -n 1        | #
                    cut -d ":" -f  6`                     #
     elif [ "$SRCTRANSFORM" == "FLIP000" ]
      then   VHASH=`echo "$HASHDUMP" | grep "^$SRCNAME" | #
                    grep "$SRCHASH"  | head -n 1        | #
                    cut -d ":" -f  7`                     #
     elif [ "$SRCTRANSFORM" == "FLIP090" ]
      then   VHASH=`echo "$HASHDUMP" | grep "^$SRCNAME" | #
                    grep "$SRCHASH"  | head -n 1        | #
                    cut -d ":" -f  8`                     #
     elif [ "$SRCTRANSFORM" == "FLIP180" ]
      then   VHASH=`echo "$HASHDUMP" | grep "^$SRCNAME" | #
                    grep "$SRCHASH"  | head -n 1        | #
                    cut -d ":" -f  9`                     #
     elif [ "$SRCTRANSFORM" == "FLIP270" ]
      then   VHASH=`echo "$HASHDUMP" | grep "^$SRCNAME" | #
                    grep "$SRCHASH"  | head -n 1        | #
                    cut -d ":" -f 10`                     #
     elif [ "$SRCTRANSFORM" == "FLOP000" ]
      then   VHASH=`echo "$HASHDUMP" | grep "^$SRCNAME" | #
                    grep "$SRCHASH"  | head -n 1        | #
                    cut -d ":" -f 11`                     #
     elif [ "$SRCTRANSFORM" == "FLOP090" ]
      then   VHASH=`echo "$HASHDUMP" | grep "^$SRCNAME" | #
                    grep "$SRCHASH"  | head -n 1        | #
                    cut -d ":" -f 12`                     #
     elif [ "$SRCTRANSFORM" == "FLOP180" ]
      then   VHASH=`echo "$HASHDUMP" | grep "^$SRCNAME" | #
                    grep "$SRCHASH"  | head -n 1        | #
                    cut -d ":" -f 13`                     #
     elif [ "$SRCTRANSFORM" == "FLOP270" ]
      then   VHASH=`echo "$HASHDUMP" | grep "^$SRCNAME" | #
                    grep "$SRCHASH"  | head -n 1        | #
                    cut -d ":" -f 14`                     #
     else    VHASH="XXX"
     fi

     echo $VHASH

  }
# --------------------------------------------------------------------------- #
  if [ -f "${TMP}.clean" ];then rm ${TMP}.clean;fi
# --------------------------------------------------------------------------- #
  ALLITEMS=`sed ':a;N;$!ba;s/\n/::NL::/g' ${IOSSRC} | #
            sed 's/\(::NL::\)\(R[0-9][0-9]C[0-9][0-9] = \)/\n\2/g'`
  NUMITEMS=`echo "$ALLITEMS" | wc -l`
  NUM=`echo "$ALLITEMS"      | # DISPLAY $ALLITEMS
       sed 's/::NL::/\n/g'   | # PUT ON NEW LINES
       egrep "[A-F0-9]{12}"  | # GREP ELEMENTS
       wc -l`                  # COUNT LINES
  if [ "$SRCMODE" == "FILE" ]
  then  echo "CHECKING    $IOSSRC ($NUM ELEMENTS)"
  else  echo "CHECKING    $SRCDIR/ ($NUM ELEMENTS)"
  fi
# --------------------------------------------------------------------------- #
  CNT=1
  while [ $CNT -le $NUMITEMS ]
   do  # ------------------------------------------------------------------ #
       ITEM=`echo "$ALLITEMS" | head -n 1`

       if [ `echo $ITEM | grep "^R" | wc -l` -gt 0 ]
       then  ITEMNAME=`echo $ITEM      | #
                       cut -d "=" -f 1 | #
                       sed 's/[ ]*$//'`  #
             ITEMLMNTS=`echo $ITEM          | #
                        sed 's/::NL::/\n/g' | #
                        cut -d "'" -f 2`      #
       # ------------------------------------------------------------------ #
       if [ "$MODUS" == "EQUAL" ]
        then
        # --------------------------------------------------------------- #
        # COMPARE ITEM ELEMENTS (UNIQ BY HASHES)
        # --------------------------------------------------------------- #
          LMNTCOMPARE=""
          for LMNT in `echo "$ITEMLMNTS" | sed 's/::NL::/\n/g'`
           do
               echo -en " $ITEMNAME   $NUM STILL TO DO \r"
               printf "\b${SP:i++%${#SP}:1}"
               LMNTNAME=`echo $LMNT | cut -d ":" -f 1`
               LMNTTRNSFRM=`echo $LMNT | cut -d ":" -f 2`
               LMNTPHASH=`showPHash $LMNTNAME $LMNTTRNSFRM`
               if [ "$LMNTPHASH" == "" ]
               then mkPHash $LMNTNAME
                    LMNTPHASH=`showPHash $LMNTNAME $LMNTTRNSFRM`
               fi
               LMNTCOMPARE="$LMNTCOMPARE|$LMNT;$LMNTPHASH"
               NUM=`expr $NUM - 1`
          done
               ITEM=`echo $LMNTCOMPARE             | # ALL ELEMENTS
                     sed 's/^|//'                  | # REMOVE LEADING '|'
                     sed 's/|/\n/g'                | # PUT ELEMENT ON SINGLE LINES
                     grep -n ""                    | # NUMBER LINES (TO SORT)
                     sort -t ';' -k 2,2 -u         | # UNIQ ACCORDING TO PHASH
                     sort -n -t ':' -k 1,1         | # RESTORE LINE ORDER
                     cut -d ':' -f 2-              | # RM LINE NUMBERS
                     cut -d ';' -f 1               | # RM PHASH
                     sed "s/^/'/"                  | # QUOTE LINES (START)
                     sed "s/$/',::NL::          /" | # QUOTE LINES (END)
                     sed ':a;N;$!ba;s/\n//g'       | # MAKE ONE LINE
                     sed "s/^/$ITEMNAME = \[/"     | # ADD $ITEMNAME (START)
                     sed "s/,::NL::[ ]*$/\];/"`      # CLOSE LIST
       # ------------------------------------------------------------------ #
        else
        # --------------------------------------------------------------- #
        # COMPARE ITEM ELEMENTS (HAMMING)
        # --------------------------------------------------------------- #
          while [ `echo $ITEMLMNTS | sed 's/ /\n/g'  | wc -l` -gt 1 ]
           do
             LMNT=`echo $ITEMLMNTS | sed 's/ /\n/g' | head -n 1`
             LMNTNAME=`echo $LMNT | cut -d ":" -f 1`
             mkPHash $LMNTNAME
             LMNTTRNSFRM=`echo $LMNT | cut -d ":" -f 2`
             LMNTPHASH=`showPHash $LMNTNAME $LMNTTRNSFRM`

             ITEMLMNTS=`echo $ITEMLMNTS | # SHOW ALL LMNTS
                        sed 's/ /\n/g'  | # PUT ON NEW LINES
                        grep -v $LMNT`    # CLEAR CURRENT LMNT

             for OTHER in `echo $ITEMLMNTS | sed 's/ /\n/g'`
              do 
                 echo -en " $ITEMNAME   $NUM STILL TO DO \r"
                 printf "\b${SP:i++%${#SP}:1}"

                 OTHERNAME=`echo $OTHER | cut -d ":" -f 1`
                 mkPHash $OTHERNAME
                 OTHERTRNSFRM=`echo $OTHER | cut -d ":" -f 2`
                 OTHERPHASH=`showPHash $OTHERNAME $OTHERTRNSFRM`

                 HMNGDSTNCE=`hamming $LMNTPHASH $OTHERPHASH`
                 if [ "$HMNGDSTNCE" -lt "$HDREQUIRED" ]
                 then 
               # ------------------------------------------------- #
               # CLEAR ELEMENT FROM ITEM
               # ------------------------------------------------- #
                 ITEM=`echo $ITEM | sed "s/,::NL::[ ]*'$OTHER'//"`
               # ------------------------------------------------- #
               # CLEAR ELEMENT FROM ITEM ELEMENTS FOR COMING LOOPS 
               # ------------------------------------------------- #
                 ITEMLMNTS=`echo $ITEMLMNTS | # SHOW ALL LMNTS
                            sed 's/ /\n/g'  | # PUT ON NEW LINES
                            grep -v $OTHER`   # IGNORE CURRENT LMNT
               # ------------------------------------------------- #
               # CLEAR ELEMENT FROM ALL ITEMS FOR COMING LOOPS 
               # ------------------------------------------------- #
                 ALLITEMS=`echo "$ALLITEMS" | #
                           sed "/$LMNT/s/,::NL::[ ]*'$OTHER'//g"`
               # ------------------------------------------------- #
                 NUM=`expr $NUM - 1`
                 fi
             done
             NUM=`expr $NUM - 1`
          done
        # --------------------------------------------------------------- #
          NUM=`echo "$ALLITEMS"      | # DISPLAY $ALLITEMS
               sed 's/::NL::/\n/g'   | # PUT ON NEW LINES
               egrep "'[A-F0-9]{12}" | # GREP ELEMENTS
               wc -l`                  # COUNT LINES
       # ------------------------------------------------------------------ #
       fi
       # ------------------------------------------------------------------ #
             echo "$ITEM" | sed 's/::NL::/\n/g' >> ${TMP}.clean
      else   echo "$ITEM" | sed 's/::NL::/\n/g' >> ${TMP}.clean
      fi
      # ----------------------------------------------------------------- #
      ALLITEMS=`echo "$ALLITEMS" | # DISPLAY $ALLITEMS
                sed "1d"`          # DELETE FIRST LINE 
      CNT=`expr $CNT + 1`
  done # ---------------------------------------------------------------- #
# --------------------------------------------------------------------------- #
  if [ "$SRCMODE" == "FILE" ]
  then  cat ${TMP}.clean  > ${IOSSRC}
        echo -en "\r"
        NUM=`egrep "'[A-F0-9]{12}" $IOSSRC | wc -l`
        echo -e "CLEANED     $IOSSRC ($NUM ELEMENTS)"
  else  echo -en "\r"
        echo -e "                                   "
  fi
# --------------------------------------------------------------------------- #
# UPDATE HTML FILES
# --------------------------------------------------------------------------- #
   IOSSRCNAME=`basename $IOSSRC`
   HTMLFILES=`grep -l $IOSSRCNAME $HTMLDIR/*.html`

   if [ "$HTMLFILES" != "" ]
   then printf "UPDATE HTML"

  # REMOVE CYCLISTE IF NECESSARY
  # ----------------------------
    for SINGLE in `grep "^R.*];$" $IOSSRC | #
                   cut -d "=" -f 1`         #
     do ( IFS=$'\n'
          for MATCH in `grep -nH ".*cycliste.*#$SINGLE" $HTMLFILES`  
           do   LN=`echo $MATCH | cut -d ":" -f 2`
              HTML=`echo $MATCH | cut -d ":" -f 1`
              sed -i "${LN}d" $HTML
          done; )
    done

  # INSERT CYCLISTE IF NECESSARY
  # ----------------------------
    for MULTI in `grep "^R" $IOSSRC  | #
                  grep -v "];$"      | #
                  cut -d "=" -f 1`     #
     do HASCYCLISTE=`grep -nH ".*cycliste.*#$MULTI" $HTMLFILES | wc -l`
        if [ "$HASCYCLISTE" -lt 1 ]
         then CCLASS=`echo $MULTI            | #
                      tr [:upper:] [:lower:] | #
                      sed 's/^.../& /'`        #
        CYCLISTE=`echo "<div class=\"$CCLASS b cycliste start\">\
                        <a href=\"#$MULTI\"></a></div>" | #
                  sed ":a;N;\$!ba;s/\n/$NL/g" | sed 's/>[ ]*</></g'`
              sed -i "s|^<div id=\"$MULTI\".*$|&\n$CYCLISTE|" $HTMLFILES
        fi
    done

  # COPY VISUAL DUPLICATE FROM ARRAY INTO HTML (AS STARTER ELEMENT) 
  # --------------------------------------------------------------
    for HTML in $HTMLFILES
     do (IFS=$'\n'; printf " $HTML "
         for E in `grep 'id="R' $HTML | #
                   grep 'background-image:url'`
          do ENAME=`echo $E | cut -d '"' -f 2`
             EID=`echo $ENAME | #
                  sed 's/[^RC0-9]//g' | #
                  md5sum | cut -c 1-4`  #
             ESRCNAME=`echo $E         | #
                       cut -d "(" -f 2 | #
                       cut -d ")" -f 1`  #
             ETRANSFORM=`echo $E | sed 's/class="/\n&/g' | #
                         grep "^class=" | head -n 1      | #
                         cut -d '"' -f 2                 | #
                         sed 's/^..........//'           | #
                         sed 's/\(fl[oi]p\)\( a\)/\1/'   | #
                         sed 's/a/ROTO/'                 | #
                         tr [:lower:] [:upper:]`           #
             ESRC="${ESRCNAME}:${ETRANSFORM}"

             ALTSRCALL=`sed ':a;N;$!ba;s/\n/::NL::/g' ${IOSSRC} | #
                        sed 's/\(::NL::\)\(R[0-9][0-9]C[0-9][0-9] = \)/\n\2/g' | #
                        grep "^$ENAME" | sed 's/::NL::/\n/g' | cut -d "'" -f 2`

             if [ `echo $ALTSRCALL | sed 's/ /\n/g' | #
                   head -n 1` != "${ESRCNAME}:${ETRANSFORM}" ]
             then
                   mkPHash $ESRCNAME
                   PHASH=`showPHash $ESRCNAME $ETRANSFORM`

                   CNT=1
                   while [ $CNT -le `echo $ALTSRCALL | #
                                     sed 's/ /\n/g'  | #
                                     wc -l` ]
                   do  
                        ALTSRC=`echo $ALTSRCALL | #
                                sed 's/ /\n/g'  | #
                                head -n $CNT    | #
                                tail -n 1`        #
                        ALTSRCNAME=`echo $ALTSRC | cut -d ":" -f 1`
                        ALTSRCTRANSFORM=`echo $ALTSRC | cut -d ":" -f 2`
                        mkPHash $ALTSRCNAME
                        ALTPHASH=`showPHash $ALTSRCNAME $ALTSRCTRANSFORM`

                        HMNGDSTNCE=`hamming $PHASH $ALTPHASH`

                        if [ "$HMNGDSTNCE" -gt $HDREQUIRED ]
                        then CNT=`expr $CNT + 1`      # KEEP LOOPING
                        else CNT=`expr $CNT + 100000` # STOP LOOPING
                        fi
                   done

                   if [ "${ALTSRC}" != "$ESRC" ] &&
                      [ "${ALTSRCNAME}" != ""  ]
                   then #echo "$ENAME"
                        ALTCLASS=`echo $ENAME$ALTSRCTRANSFORM | #
                                  sed 's/C[0-9][0-9]/ & K /'  | #
                                  sed 's/FL[IO]P/&ROTO/'      | #
                                  sed 's/ROTO/ A/'            | #
                                  tr [:upper:] [:lower:]      | #
                                  tr -s ' '`                    #
                        ALTHTML=`echo "<div id=\"$ENAME\" \
                                 style=\"background-image:url($ALTSRCNAME)\" \
                                 class=\"$ALTCLASS\"></div>" | #
                                 tr -s ' '`
                        LN=`grep -n "id=\"${ENAME}\"" $HTML | cut -d ":" -f 1`
                        sed -i "${LN}s,^.*$,$ALTHTML," $HTML
                   fi
             fi

         done;)
    done
  else echo -e "\e[31mNO HTML TO UPDATE\e[0m"
  fi

# CLEAN UP
# --------------------------------------------------------------------------- #
  if [ `echo ${TMP} | wc -c` -ge 4 ] &&
     [ `ls ${TMP}*.* 2>/dev/null | wc -l` -gt 0 ]
  then  rm ${TMP}*.* ;fi;echo -e "\n"

exit 0;
