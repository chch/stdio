#!/bin/bash

# OUTPUTS ALL POSSIBLE (ROTATE/FLIP) CONNECTIONS
# https://stackoverflow.com/q/19408649/
# -> pipe-input-into-a-script
  read ELEMENT
# ----------------------------------------------------------------- #
# TODO: COMPENSATE DIFFERENT FORMAT (E.G. CONNECTORS ONLY)
  C=`echo $ELEMENT | cut -d ":" -f 1`;
  T=`echo $ELEMENT | cut -d ":" -f 2`;
# --
  function c(){ echo $C | cut -d "_" -f 2- | cut -d "_" -f $1; }
# ----------------------------------------------------------------- #
  if [ "$T" = ROTO000 ];then C1=`c 1`;C2=`c 2`;C3=`c 3`;C4=`c 4`;fi
  if [ "$T" = ROTO090 ];then C1=`c 4`;C2=`c 1`;C3=`c 2`;C4=`c 3`;fi
  if [ "$T" = ROTO180 ];then C1=`c 3`;C2=`c 4`;C3=`c 1`;C4=`c 2`;fi
  if [ "$T" = ROTO270 ];then C1=`c 2`;C2=`c 3`;C3=`c 4`;C4=`c 1`;fi
  if [ "$T" = FLIP000 ];then C1=`c 1`;C2=`c 4`;C3=`c 3`;C4=`c 2`;fi
  if [ "$T" = FLIP090 ];then C1=`c 4`;C2=`c 3`;C3=`c 2`;C4=`c 1`;fi
  if [ "$T" = FLIP180 ];then C1=`c 3`;C2=`c 2`;C3=`c 1`;C4=`c 4`;fi
  if [ "$T" = FLIP270 ];then C1=`c 2`;C2=`c 1`;C3=`c 4`;C4=`c 3`;fi
  if [ "$T" = FLOP000 ];then C1=`c 3`;C2=`c 2`;C3=`c 1`;C4=`c 4`;fi
  if [ "$T" = FLOP090 ];then C1=`c 2`;C2=`c 1`;C3=`c 4`;C4=`c 3`;fi
  if [ "$T" = FLOP180 ];then C1=`c 1`;C2=`c 4`;C3=`c 3`;C4=`c 2`;fi
  if [ "$T" = FLOP270 ];then C1=`c 4`;C2=`c 3`;C3=`c 2`;C4=`c 1`;fi
# ----------------------------------------------------------------- #
  ROTO000=${C1}_${C2}_${C3}_${C4};ROTO090=${C2}_${C3}_${C4}_${C1}
  ROTO180=${C3}_${C4}_${C1}_${C2};ROTO270=${C4}_${C1}_${C2}_${C3}
  FLIP000=${C1}_${C4}_${C3}_${C2};FLIP090=${C4}_${C3}_${C2}_${C1}
  FLIP180=${C3}_${C2}_${C1}_${C4};FLIP270=${C2}_${C1}_${C4}_${C3}
 #FLOP000=${C3}_${C2}_${C1}_${C4};FLOP090=${C2}_${C1}_${C4}_${C3}
 #FLOP180=${C1}_${C4}_${C3}_${C2};FLOP270=${C4}_${C3}_${C2}_${C1}
# ================================================================= #
  echo "$ROTO000 $ROTO090 $ROTO180 $ROTO270" \
       "$FLIP000 $FLIP090 $FLIP180 $FLIP270" | #
  sed 's/ /\n/g' | sort                      | #
  sed ":a;N;\$!ba;s/\n/:/g"

exit 0;
