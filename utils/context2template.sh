#!/bin/bash

# =========================================================================== #
# RE-CREATE I/O TEMPLATE FROM CONTEXT                                         #
# =========================================================================== #
# Copyright (C) 2020 Christoph Haag                                           #
# --------------------------------------------------------------------------- #
# This is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# 
# The software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License below for more details.
# -> http://www.gnu.org/licenses/gpl.txt

# =========================================================================== #
# C O N F I G U R E
# =========================================================================== #
  SVGDIR="../src/ioio";TMP="/tmp/TMP"
# --------------------------------------------------------------------------- #
  HTML=`echo $* | sed 's/ /\n/g' | grep "\.html$" | head -n 1`
  TARGET=`echo $* | sed 's/ /\n/g' | grep "\.svg$" | head -n 1`
  CONTEXT=`echo $* | sed 's/ /\n/g' | #
           grep '^R[0-9][0-9]C[0-9][0-9]$' | head -n 1`
# --------------------------------------------------------------------------- #
  if [ ! -f $HTML ] || [ "$HTML" == "" ] ||
     [ ! -f $TARGET ] || [ "$TARGET" == "" ] ||
     [ "$CONTEXT" == "" ]
  then echo "PLEASE PROVIDE INPUT/CONF FILE"
       echo "./context2template.sh ../_/in.html out.svg R05C05"
       exit 1;fi
# --------------------------------------------------------------------------- #
  ROWS=3;COLS=3 
  STEPX="335";STEPY="335";SCALE="1"
# --------------------------------------------------------------------------- #
  INKSCAPEEXTENSION="../lib/python"
  export PYTHONPATH="$INKSCAPEEXTENSION"
  APPLYTRANSFORMPY="../lib/python/applytransform.py"
  APPLYTRANSFORMPY=`realpath $APPLYTRANSFORMPY`
# --------------------------------------------------------------------------- #
  R=`echo $CONTEXT | cut -d 'C' -f 1 | sed 's/[^0-9]*//g' | sed 's/^0*//'`
  C=`echo $CONTEXT | cut -d 'C' -f 2 | sed 's/[^0-9]*//g' | sed 's/^0*//'`
  RMIN=`python -c "print $R - ($ROWS-1)/2"`
  RMAX=`python -c "print $R + ($ROWS-1)/2"`
  CMIN=`python -c "print $C - ($COLS-1)/2"`
  CMAX=`python -c "print $C + ($COLS-1)/2"`
  XPOS=`python -c "print ($R - $RMIN) * $STEPX * -1"`
  YPOS=`python -c "print ($C - $CMIN) * $STEPY * -1"`
# =========================================================================== #
  echo '<?xml version="1.0" encoding="UTF-8" standalone="no"?>
        <svg width="100" height="100" id="svg" version="1.1"
         xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape">' | #
  tr -s ' ' | tee ${TMP}.1.tmp > ${TMP}.2.tmp # START TO TMP FILES
# --------------------------------------------------------------------------- #
 (IFS=$'\n'
  for RX in `seq $RMIN $RMAX       | #
             sed 's/^/00/'         | #
             rev | cut -c 1-2 | rev`
   do
 # ------------------------------------------------------------------------- #
   for CX in `seq $CMIN $CMAX       | #
              sed 's/^/00/'         | #
              rev | cut -c 1-2 | rev` #
    do RXCX="R${RX}C${CX}"
  # ------------------------------------------------------------------ #
       if [ "$RXCX" == "$CONTEXT" ]
       then COLLECT=${TMP}.2.tmp
       else COLLECT=${TMP}.1.tmp
       fi
     # --------------------------------------------------------------- #
       E=`grep "div id=\"$RXCX\"" $HTML`
       T=`echo $E | sed 's/^.*class="//' | #
          cut -d "\"" -f 1 | sed 's/ /\n/g' | #
          egrep "^fl|^a" | sed 's/^a//'`   #
       ANGLE=`echo $T | sed 's/ /\n/g' | sed '/[^0-9]\+/d'`
       FLIOP=`echo $T | sed 's/ /\n/g' | sed '/[^a-z]\+/d'`
       E=`echo $E |  cut -d "(" -f 2  | #
          cut -d ")" -f 1             | # 
          rev | cut -d "/" -f 1 | rev | # EXTRACT BASENAME
          sed 's/$/.svg/'`              # ADD .svg EXTENSION
     # --------------------------------------------------------------- #
       ESRC="$SVGDIR/${E}"
       if [ -f $ESRC ];then
     # --------------------------------------------------------------- #
       X=$XPOS;Y=$YPOS;SCALE=1;ROTATE=0
     # --------------------------------------------------------------- #
       if [ "$ANGLE" == "090" ];then ROTATE=90;X=`expr $X + 400`;fi
       if [ "$ANGLE" == "180" ];then ROTATE=180
                                     X=`expr $X + 400`
                                     Y=`expr $Y + 400`;fi
       if [ "$ANGLE" == "270" ];then ROTATE=270;Y=`expr $Y + 400`;fi
     # ---------------
       if [ "$FLIOP" == "flip" ] # -- FLIP HORIZONTAL -------------- #
       then SCALE="-$SCALE $SCALE"
            if   [ "$ANGLE" == "000" ];then X=`expr $X + 400`
            elif [ "$ANGLE" == "090" ];then X=`expr $X - 400`
            elif [ "$ANGLE" == "180" ];then X=`expr $X - 400`
            elif [ "$ANGLE" == "270" ];then X=`expr $X + 400`
            fi
       fi
     # ---------------
       if [ "$FLIOP" == "flop" ] # -- FLIP VERTICAL ---------------- #
       then SCALE="$SCALE -$SCALE"
            if   [ "$ANGLE" == "000" ];then Y=`expr $Y + 400`
            elif [ "$ANGLE" == "090" ];then Y=`expr $Y + 400`
            elif [ "$ANGLE" == "180" ];then Y=`expr $Y - 400`
            elif [ "$ANGLE" == "270" ];then Y=`expr $Y - 400`
            fi
       fi
     # --------------------------------------------------------------- #
       TRANSFORM="\"translate($X,$Y) scale($SCALE) rotate($ROTATE)\""
     # --------------------------------------------------------------- #
       echo "<g transform=$TRANSFORM>"        >> $COLLECT # OPEN GROUP
       python $APPLYTRANSFORMPY $ESRC         | # ...
       sed ':a;N;$!ba;s/\n//g'                | # REMOVE ALL LINEBREAKS
       sed 's/<g/\n&/g'                       | # MOVE GROUP TO NEW LINES
       sed '/groupmode="layer"/s/<g/::LG::/g' | # PLACEHOLDER FOR LAYERGROUP OPEN
       sed ':a;N;$!ba;s/\n/ /g'               | # REMOVE ALL LINEBREAKS
       sed 's/::LG::/\n<g/g'                  | # RESTORE LAYERGROUP OPEN + NEWLINE
       sed 's/display:none/display:inline/g'  | # MAKE VISIBLE EVEN WHEN HIDDEN
       grep -v 'label="XX_'                   | # REMOVE EXCLUDED LAYERS
       sed 's/<\/svg>/\n&/g'                  | # CLOSE TAG ON SEPARATE LINE
       sed "s/^[ \t]*//"                      | # REMOVE LEADING BLANKS
       grep "^<g"                             | # GROUPS (=LAYERS) ONLY
       sed 's/<\/*g[^>]*>//g'                 | # REMOVE GROUPS
       tr -s ' '                              | # REMOVE CONSECUTIVE BLANKS
       tee                                    >> $COLLECT # APPEND TO FILE
       echo "</g>"                            >> $COLLECT # CLOSE GROUP
     # --------------------------------------------------------------- #
       fi
 # ------------------------------------------------------------------------- #
   XPOS=`python -c "print $XPOS + $STEPX"`
 # ------------------------------------------------------------------------- #
   done
 # ------------------------------------------------------------------------- #
   YPOS=`python -c "print $YPOS + $STEPY"`
   XPOS=`python -c "print ($C - $CMIN) * $STEPX * -1"`
# --------------------------------------------------------------------------- #
  done;)
# =========================================================================== #
  echo "</svg>" >> ${TMP}.1.tmp
  echo "</svg>" >> ${TMP}.2.tmp
  sed -i 's/<\/svg>//' $TARGET
# --------------------------------------------------------------------------- #
 for COLLECT in ${TMP}.1.tmp ${TMP}.2.tmp
  do sed ":a;N;\$!ba;s/\n/$B/g" $COLLECT | #
     sed 's/<path/\n&/g' > ${TMP}.0.tmp
  (IFS=$'\n'
  # ----------------------------------------------------------------------- #
   for P in `grep -n '^<path' ${TMP}.0.tmp`
    do
       LN=`echo $P | cut -d ":" -f 1`
       PSTYLE=`echo $P | sed 's/style="/\n&/' | #
               grep '^style=' | cut -d '"' -f 2`
       FILL=`echo $PSTYLE | sed 's/;/\n/g' | egrep '^fill:'`
       STROKE=`echo $PSTYLE | sed 's/;/\n/g' | egrep '^stroke:'`
       if [ "$STROKE" != "stroke:none" ]
       then  STROKEWIDTH=`echo $PSTYLE | #
                          sed 's/;/\n/g' | #
                          egrep '^stroke-width:' | #
                          sed 's/ke-width:[0-9\.]\+[a-z]*/ke-width:1px/g'`
              STROKEJOIN=`echo $PSTYLE | #
                          sed 's/;/\n/g' | #
                          egrep '^stroke-linejoin:'`
               STROKECAP=`echo $PSTYLE | #
                          sed 's/;/\n/g' | #
                          egrep '^stroke-linecap:'`
       else STROKEWIDTH="";STROKEJOIN="";STROKECAP=""
       fi
       NEWSTYLE="$FILL;$STROKE;$STROKEWIDTH;$STROKEJOIN;$STROKECAP"
       NEWSTYLE=`echo $NEWSTYLE | sed 's/^;*//' | sed 's/;*$//'`
       sed -i "${LN}s/$PSTYLE/$NEWSTYLE/g"               ${TMP}.0.tmp
 
   done;)
  # ----------------------------------------------------------------------- # 
    sed -i "s/fill:#ffffff/::F:FFFF::/gI"              ${TMP}.0.tmp
    sed -i "s/fill:#[0-9a-f]\{6\}/fill:#cccccc/gI"     ${TMP}.0.tmp
    sed -i "s/::F:FFFF::/fill:#ffffff/g"               ${TMP}.0.tmp
    sed -i "s/stroke:#ffffff/::S:FFFF::/gI"            ${TMP}.0.tmp
    sed -i "s/stroke:#[0-9a-f]\{6\}/stroke:#b3b3b3/gI" ${TMP}.0.tmp
    sed -i "s/::S:FFFF::/stroke:#ffffff/g"             ${TMP}.0.tmp
    sed ":a;N;\$!ba;s/\n//g" ${TMP}.0.tmp | sed "s/::B::/\n/g" > $COLLECT
  # ----------------------------------------------------------------------- # 
 done
# --------------------------------------------------------------------------- #
  LAYERNAME="XX_${CONTEXT}-AROUND"
  echo "<g inkscape:groupmode=\"layer\"   \
           id=\"$LAYERNAME\"              \
           sodipodi:insensitive=\"true\"  \
           inkscape:label=\"$LAYERNAME\">" | #
  tr -s ' '                                >> $TARGET
  python $APPLYTRANSFORMPY ${TMP}.1.tmp    | #
  sed ':a;N;$!ba;s/\n/::L::/g'             | #
  sed 's/<g/\n&/g'                         | #
  sed 's/<\/svg>/\n&/g'                    | #
  grep '^<g'                               | #
  sed 's/::L::/\n/g'                       | #
  sed 's/<\/*g[^>]*>//g'                   >> $TARGET
  echo '</g>'                              >> $TARGET
 
  LAYERNAME="XX_${CONTEXT}"
  echo "<g inkscape:groupmode=\"layer\"   \
           id=\"$LAYERNAME\"              \
           style=\"display:none\"         \
           sodipodi:insensitive=\"true\"  \
           inkscape:label=\"$LAYERNAME\">" | #
  tr -s ' '                                >> $TARGET
  python $APPLYTRANSFORMPY ${TMP}.2.tmp    | #
  sed ':a;N;$!ba;s/\n/::L::/g'             | #
  sed 's/<g/\n&/g'                         | #
  sed 's/<\/svg>/\n&/g'                    | #
  grep '^<g'                               | #
  sed 's/::L::/\n/g'                       | #
  sed 's/<\/*g[^>]*>//g'                   >> $TARGET
  echo '</g>'                              >> $TARGET

  echo '</svg>'                            >> $TARGET
# --------------------------------------------------------------------------- #
# CLEAN UP
# --------------------------------------------------------------------------- #
  if [ `echo ${TMP} | wc -c` -ge 4 ] &&
     [ `ls ${TMP}*.* 2>/dev/null | wc -l` -gt 0 ]
  then  rm ${TMP}*.* ;fi

exit 0;
