#!/bin/bash

# CREATE SVG FROM (HTML) DUMP                                                 #
# --------------------------------------------------------------------------- #
# Copyright (c) 2016 Christoph Haag                                           #
# --------------------------------------------------------------------------- #

# This is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# 
# The software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License below for more details.
# 
# -> http://www.gnu.org/licenses/gpl.txt

# =========================================================================== #
# DEFINE STANDARDS
# --------------------------------------------------------------------------- #
  SVGDIR="src/ioio";TMP="/tmp/STDIO${RANDOM}"
  SVGW="1052";SVGH="1488"
  CONTENTSCALE="0.8";STROKEWIDTH="1.0";RFILL="#b7c8c4";OFFSETX="0";OFFSETY="0"
# =========================================================================== #
# USER INTERACTION
# --------------------------------------------------------------------------- #
  HTML=$1 ; SVGOUT=`echo ${HTML} | sed 's/\.html$/.svg/'`
# --------------------------------------------------------------------------- #
  if [ ! -f $HTML ] ||
     [ `echo "$HTML" | wc -c` -lt 2 ]
   then echo; echo "A INPUT PLEASE!"
              echo "e.g. $0 _/20161110212041.html"; echo
      exit 0;
  fi
  if [ -f $SVGOUT ]
   then echo "$SVGOUT DOES EXIST"
        read -p "OVERWRITE $SVGOUT? [y/n] " ANSWER
        if [ "$ANSWER" != y ];then echo "BYE BYE"; exit 1; else echo; fi
  fi
# --------------------------------------------------------------------------- #
  if [ `echo $* | sed 's/ -/\n-/g' | #
        egrep -- "^[-]+template"  | wc -l` -gt 0 ]
  then  TEMPLATE=`echo $* | sed 's/ -/\n-/g'   | #
                  egrep -- "^[-]+template"    | #
                  head -n 1 | cut -d "=" -f 2-` #
        if  [ ! -f $TEMPLATE ]
         then echo "TEMPLATE DOES NOT EXIST"
              read -p "PROCEED WITH STANDARD VALUES? [y/n] " ANSWER
              if [ "$ANSWER" != y ];then echo "BYE BYE"; exit 1;fi
        fi
   if  [ -f $TEMPLATE ]
    then SVGW=`grep 'width="'  $TEMPLATE | head -n 1 | cut -d '"' -f 2`
         SVGH=`grep 'height="' $TEMPLATE | head -n 1 | cut -d '"' -f 2`
         CONTENTROTATE=`sed 's/ /\n/g' $TEMPLATE   | #
                        grep CONTENTROTATE         | #
                        cut -d ":" -f 2            | #
                        sed '/[^0-9\.-]\+/d'       | # 
                        head -n 1`                   #
         TEMPLATESCALE=`sed 's/ /\n/g' $TEMPLATE   | #
                        grep CONTENTSCALE          | #
                        cut -d ":" -f 2            | #
                        sed '/[^0-9\.]\+/d'        | # 
                        head -n 1`                   #
         TEMPLATESTROKE=`sed 's/ /\n/g' $TEMPLATE  | #
                         grep STROKEWIDTH          | #
                         cut -d ":" -f 2           | #
                         sed '/[^0-9\.]\+/d'       | # 
                         head -n 1`                  #
         TEMPLATEOFFSETX=`sed 's/ /\n/g' $TEMPLATE | #
                          grep OFFSETX             | #
                          cut -d ":" -f 2          | #
                          sed '/[^0-9\.\-]\+/d'    | # 
                          head -n 1`                 #
         TEMPLATEOFFSETY=`sed 's/ /\n/g' $TEMPLATE | #
                          grep OFFSETY             | #
                          cut -d ":" -f 2          | #
                          sed '/[^0-9\.\-]\+/d'    | # 
                          head -n 1`                 #
   fi
  fi
      if [ `echo $* | sed 's/ -/\n-/g' | egrep -- "^[-]+scale" | wc -l` -gt 0 ]
      then  TEMPLATESCALE=`echo $* | sed 's/ /\n/g' | #
                           egrep -- "^[-]+scale"    | #
                           cut -d '=' -f 2`
      fi

      if [ "$CONTENTROTATE"   == "" ];then CONTENTROTATE="0";fi
      if [ "$TEMPLATEOFFSETX" != "" ];then OFFSETX="$TEMPLATEOFFSETX";fi
      if [ "$TEMPLATEOFFSETY" != "" ];then OFFSETY="$TEMPLATEOFFSETY";fi
      if [ "$TEMPLATESCALE"   != "" ];then CONTENTSCALE="$TEMPLATESCALE";fi
      if [ "$TEMPLATESTROKE"  != "" ];then STROKEWIDTH="$TEMPLATESTROKE";fi

  if [ `echo $* | sed 's/--/\n--/g' | #
        grep -- "^--addref" | wc -l` -gt 0 ]
  then  ADDREF="YES"
  fi

  THISCMD=`echo "$0 $*" | sed 's/--/-/g'   | #
           sed 's/^/<!-- /' | sed 's/$/ -->/'` #
  SRCSTAMP=`grep '<!-- ./mkpossible.sh' $HTML | head -n 1`

# =========================================================================== #
# INCLUDE
# --------------------------------------------------------------------------- #
  source lib/sh/shuffle.functions

  INKSCAPEEXTENSION="lib/python"
  export PYTHONPATH="$INKSCAPEEXTENSION"
  APPLYTRANSFORMPY="lib/python/applytransform.py"
  APPLYTRANSFORMPY=`realpath $APPLYTRANSFORMPY`
# =========================================================================== #
# DO IT NOW!
# =========================================================================== #

  printf "PREPARING SVG INPUT "

( IFS=$'\n'
  for E in `grep '<div' $HTML                   | #
            grep 'id="R[0-9][0-9]C[0-9][0-9]"'  | #
            shuf --random-source=<(mkseed $HTML)` #
   do
     ID=`echo $E  | cut -d "\"" -f 2`   #
     SVGNAME=`echo $E |  cut -d "(" -f 2  | #
              cut -d ")" -f 1`              # EXTRACT BASENAME
     SVG="${SVGDIR}/${SVGNAME}.svg"
     TMPSVG=${TMP}${ID}`echo $SVGNAME | sed 's/\.svg$//'` # RM EXTENSIONS   
   # -------------------------------------------------------------------- #
     printf . # INCREASE STATUS
   # -------------------------------------------------------------------- #
   # MOVE ALL LAYERS ON SEPARATE LINES IN A TMP FILE
   # -------------------------------------------------------------------- #
     sed ':a;N;$!ba;s/\n//g' $SVG           | # REMOVE ALL LINEBREAKS
     sed 's/<g/\n&/g'                       | # MOVE GROUP TO NEW LINES
     sed '/groupmode="layer"/s/<g/4Fgt7R/g' | # PLACEHOLDER FOR LAYERGROUP OPEN
     sed ':a;N;$!ba;s/\n/ /g'               | # REMOVE ALL LINEBREAKS
     sed 's/4Fgt7R/\n<g/g'                  | # RESTORE LAYERGROUP OPEN + NEWLINE
     sed 's/display:none/display:inline/g'  | # MAKE VISIBLE EVEN WHEN HIDDEN
     grep -v 'label="XX_'                   | # REMOVE EXCLUDED LAYERS
     sed 's/<\/svg>/\n&/g'                  | # CLOSE TAG ON SEPARATE LINE
     sed "s/^[ \t]*//"                      | # REMOVE LEADING BLANKS
     grep "^<g"                             | # GROUPS (=LAYERS) ONLY
     tr -s ' '                              | # REMOVE CONSECUTIVE BLANKS
     tee > ${TMPSVG}.1.tmp                    # WRITE TO TEMPORARY FILE
   # -------------------------------------------------------------------- #
   # COLLECT DEFS
   # -------------------------------------------------------------------- #
     NLFOO=Nn${RANDOM}lL
     sed  ":a;N;\$!ba;s/\n/$NLFOO/g" $SVG | # PLACEHOLDER FOR LINEBREAKS
     sed  "s,<defs,\n<defs,g"     | #
     sed "s,</defs>,</defs>\n,g"  | #
     sed '/<\/defs>/!s/\/>/&\n/g' | # IF NO CLOSE TAG BREAK AFTER FIRST />
     grep "^<defs" | head -n 1    | # SELECT DEFS (TAKE FIRST(BRUTE))
     sed 's/<[^>]*>//' | rev | # UNTAG (open)
     sed 's/>[^<]*<//' | rev | # UNTAG (close)
     sed  "s/$NLFOO//g" | tr -s ' '         | # RM LINEBREAKS
     sed  '/^[ \t]*$/d'  >>  ${TMP}.defs.tmp

   # -------------------------------------------------------------------- #
   # REMOVE LAYER GROUPING AND COPY ZPOS (IF EXISTING)
   # -------------------------------------------------------------------- #
     if [ -f ${TMPSVG}.tmp ]; then rm ${TMPSVG}.tmp ;fi

     for LAYER in `cat ${TMPSVG}.1.tmp`
      do

       LAYERZPOS=`echo $LAYER             | #
                  cut -d ">" -f 1         | # LAYER TAG ONLY
                  sed "s/zpos=/\nzpos=/g" | #
                  grep ^zpos              | #
                  head -n 1               | #
                  cut -d " " -f 1         | #
                  cut -d "\"" -f 2        | #
                  sed 's/$/000/'`           #

       if [ `echo $LAYERZPOS | wc -c` -gt 4 ]
       then ZPOS=" zpos=\"$LAYERZPOS\" ";else ZPOS="";fi 

       for ELEMENT in `echo $LAYER                            | #
                       sed 's/<g.*groupmode=\"layer[^>]*>//g' | #
                       rev | sed 's/>g\/<//' | rev            | # RM LAST </g>
                       sed "s/</\n</g"`
        do if   [ `echo $ELEMENT | grep "zpos" | wc -l` -gt 0 ]
           then    echo $ELEMENT       >> ${TMPSVG}.tmp
           elif [ `echo $LAYERZPOS | wc -c` -gt 1 ]
           then printf -- "$LAYERZPOS " >> ${TMP}.zpos # ADD STANDARD ZPOS  
                if [ `grep -s -- "^${LAYERZPOS} " \
                                      ${TMP}.zpos | #
                      wc -l` -gt 0 ];then

                CONTINUEZPOS=`grep -- "^${LAYERZPOS} " \
                                           ${TMP}.zpos   | # FIND ZPOS
                              tail -n 2                  | # SELECT LAST 2 (PREV+NOW)
                              head -n 1                  | # SELECT PREV
                              sed 's/[ ]$//'             | # RM SPACES AT THE END
                              rev | cut -d " " -f 1 | rev` # SELECT LAST FIELD
                LAYERZPOS=`expr $CONTINUEZPOS + 1`
                fi 
                ZPOS=" lzpos=\"$LAYERZPOS\" "
                printf -- "$LAYERZPOS " >> ${TMP}.zpos # ADD NEW ZPOS
   
                echo $ELEMENT | #
                sed "s,/>,$ZPOS&,g" >> ${TMPSVG}.tmp
                LAYERZPOS=`expr $LAYERZPOS + 1`
           else echo $ELEMENT | #
                sed "s,/>,$ZPOS&,g" >> ${TMPSVG}.tmp
           fi
       done

       echo >> ${TMP}.zpos # LINEBREAK

     done
     rm ${TMPSVG}.1.tmp
  done; )

# =========================================================================== #
  printf "\nLAYOUT SVG          "
# =========================================================================== #
  STEPX="335" ; STEPY="335"
# --------------------------------------------------------------------------- #
# CALCULATE VALUES 
# --------------------------------------------------------------------------- #
  COLS=`grep '<div' $HTML | grep 'id="R[0-9][0-9]C[0-9][0-9]"'   | #
        tail -n 1 | cut -d "\"" -f 2 | cut -c 5-6 | sed 's/^0//'`  #
  ROWS=`grep '<div' $HTML | grep 'id="R[0-9][0-9]C[0-9][0-9]"'   | #
        tail -n 1 | cut -d "\"" -f 2 | cut -c 2-3  | sed 's/^0//'` #
  CONTENTW=`python -c "print ($COLS * $STEPX) + 65"`
  CONTENTH=`python -c "print ($ROWS * $STEPY) + 65"`
# ----
  if [ "$CONTENTW" -gt "$CONTENTH" ]
  then SCALEMATCH=`python -c "print $SVGW / ${CONTENTW}.0"`
  else SCALEMATCH=`python -c "print $SVGH / ${CONTENTH}.0"`
  fi

  SCALEPLACE=`python -c "print $SCALEMATCH * $CONTENTSCALE"`
  SW="$STROKEWIDTH"

  CONTENTX=`python -c "print ($SVGW-$CONTENTW*$SCALEPLACE) / 2.00 + $OFFSETX"`
  CONTENTY=`python -c "print ($SVGH-$CONTENTH*$SCALEPLACE) / 2.15 + $OFFSETY"`

  if [ "$CONTENTROTATE" != 0  ]
  then  RX=`python -c "print $SVGW/2"`
        RY=`python -c "print $SVGH/2"`
        CONTENTROTATE="$CONTENTROTATE $RX $RY"
  fi
# --------------------------------------------------------------------------- #
# WRITE SVG HEADER  
# --------------------------------------------------------------------------- #
  function w() { echo $* >> $SVGOUT; };if [ -f $SVGOUT ]; then rm $SVGOUT ;fi

  w '<?xml version="1.0" encoding="UTF-8" standalone="no"?>'
  w "<svg width=\"$SVGW\" height=\"$SVGH\" id=\"svg\" version=\"1.1\""
  w ' xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"'
  w ' xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd">'
  w '<defs id="defs">'; sort -u ${TMP}.defs.tmp >> $SVGOUT ; w '</defs>'
  w "<g transform=\"rotate($CONTENTROTATE)\">"
  w "<g transform=\"translate($CONTENTX $CONTENTY) scale($SCALEPLACE)\">"
# --------------------------------------------------------------------------- #
# WRITE ELEMENTS  
# --------------------------------------------------------------------------- #
( IFS=$'\n'
  for E in `grep '<div' $HTML | grep 'id="R[0-9][0-9]C[0-9][0-9]"'`
   do
      ID=`echo $E  | cut -d "\"" -f 2`   #
       R=`echo $ID | cut -c 2-3 | sed 's/^0//'`
       C=`echo $ID | cut -c 5-6 | sed 's/^0//'`
       T=`echo $E | sed 's/^.*class="//' | #
          cut -d "\"" -f 1 | sed 's/ /\n/g' | #
          egrep "^fl|^a" | sed 's/^a//'`   #
       ANGLE=`echo $T | sed 's/ /\n/g' | sed '/[^0-9]\+/d'`
       FLIOP=`echo $T | sed 's/ /\n/g' | sed '/[^a-z]\+/d'`

       E=`echo $E |  cut -d "(" -f 2  | #
          cut -d ")" -f 1             | # 
          rev | cut -d "/" -f 1 | rev | # EXTRACT BASENAME
          sed 's/\.gif$/.svg/'`         # 
       X=`python -c "print ($C - 1) * $STEPY"`
       Y=`python -c "print ($R - 1) * $STEPY"`

   # -------------------------------------------------------------- #
     printf . # INCREASE STATUS
   # -------------------------------------------------------------- #
      SCALE=1 ; ROTATE=0
      if [ "$ANGLE" == "090" ];then ROTATE=90
                                    X=`expr $X + 400`
      fi
      if [ "$ANGLE" == "180" ];then ROTATE=180
                                    X=`expr $X + 400`
                                    Y=`expr $Y + 400`
      fi
      if [ "$ANGLE" == "270" ];then ROTATE=270
                                    Y=`expr $Y + 400`
      fi
    # FLIP HORIZONTAL
    # ---------------
      if [ "$FLIOP" == "flip" ]
       then SCALE="-$SCALE $SCALE"
            if   [ "$ANGLE" == "000" ];then X=`expr $X + 400`
            elif [ "$ANGLE" == "090" ];then X=`expr $X - 400`
            elif [ "$ANGLE" == "180" ];then X=`expr $X - 400`
            elif [ "$ANGLE" == "270" ];then X=`expr $X + 400`
            fi
      fi
    # FLIP VERTICAL
    # -------------
      if [ "$FLIOP" == "flop" ]
       then SCALE="$SCALE -$SCALE"
            if   [ "$ANGLE" == "000" ];then Y=`expr $Y + 400`
            elif [ "$ANGLE" == "090" ];then Y=`expr $Y + 400`
            elif [ "$ANGLE" == "180" ];then Y=`expr $Y - 400`
            elif [ "$ANGLE" == "270" ];then Y=`expr $Y - 400`
            fi
      fi
   # -------------------------------------------------------------- #
      TRANSFORM="\"translate($X,$Y) scale($SCALE) rotate($ROTATE)\""

      CONTENT=${TMP}${ID}${E}.tmp
      LAYERNAME="$ID"

      w "<g inkscape:groupmode=\"layer\""
      w "id=\"$LAYERNAME\""
      w "transform=$TRANSFORM"
      w "inkscape:label=\"$LAYERNAME\">"

      if [ "$ADDREF" != "" ]
       then RFILL=`echo "#d7f4ee|#b7c8c4" | #
                   sed 's/|/\n/g'         | #
                   grep -v "$RFILL"       | #
                   shuf -n 1`               #
            w "<path id=\"${LAYERNAME}EREF\""
            w "      d=\"m 0,0 400,0 0,400 -400,0 z\""
            w "      style=\"fill:$RFILL;stroke:none\"/>"
      fi

      cat $CONTENT 2> /dev/null | # USELESS USE OF CAT
      sed "s/ id=\"/&$ID/g"     | # UNIQUE ID
      tee >> $SVGOUT              # WRITE TO FILE
      w "</g>"

  done; )

  w '</g>'
  w '</g>'
  w '</svg>'

# =========================================================================== #
# UNLAYER AND ADAPT ZPOS
# =========================================================================== #
  echo -e "\nUNLAYER SVG"
# --------------------------------------------------------------------------- #
  UNLAYERED=${TMP}.UNLYRD.svg
  UNLAYEREDSORTED=${TMP}.UNLYSO.svg

  cp $SVGOUT $UNLAYERED

# COPY CLIPPING MASKS INTO GROUPS BEFORE UNGROUPING
# ----
( IFS=$'\n'
  for CLIPGROUP in `sed ':a;N;$!ba;s/\n/ /g' $UNLAYERED     | #
                    sed 's/<g/\n<g/g' | sed 's/<\/g>/&\n/g' | #
                    grep clip-path` #
   do for CLIPPATH in `echo $CLIPGROUP             | #
                       grep -v 'groupmode="layer"' | # IGNORE LAYER GROUPS
                       sed 's/ /\n/g'              | #
                       sed 's/clip-path/\n&/'      | #
                       grep "^clip-path"           | #
                       sed 's/>$//'`                 #
        do for ID in `echo $CLIPGROUP | #
                      sed 's/ /\n/g'  | #
                      grep "^id="`
            do sed -i "/clip-path/!s/$ID/& $CLIPPATH/" $UNLAYERED
           done
       done
  done; )

  sed -i "s/inkscape:groupmode=\"layer\"//g" $UNLAYERED
  python $APPLYTRANSFORMPY $UNLAYERED  | #
  sed 's/<\/*g[^>]*>//g'               | #
  sed '/^[ ]*$/d'                      | #                     
  sed "s/stroke-width:[0-9.e\-]*\(px\)*/stroke-width:${SW}px/g" > ${TMP}.tmp
  mv ${TMP}.tmp $UNLAYERED

# --------------------------------------------------------------------------- #
  printf "ADAPT ZPOS          "

( IFS=$'\n'
  for ELEMENT in `cat $UNLAYERED                     | # USELESS USE OF CAT 
                  sed ':a;N;$!ba;s/\n//g'            | # RM ALL NEWLINES (=NL)
                  sed 's,</*svg[^>]*>,,g'            | # RM SVG TAGS
          sed "s,<defs,\nXXX<defs,g"              | # MARK+SEPARATE DEFS
          sed "s,</defs>,</defs>\n,g"             | # ...
          sed "/<\/defs>/!s/\/>/&\n/g"            | # ...
          sed "/^XXX.*/d"                         | # RM MARKED
          sed "s,</sodipodi:[^>]*>,&\n,g"         | # MARK+SEPARATE SODIPODI*
          sed "s,<.\?sodipodi,\nXXX&,g"           | # ...
          sed "/<\/sodipodi:[^>]*>/!s/\/>/&\n/g"  | # ...
          sed "/^XXX.*/d"                         | # RM MARKED
            sed "s/<g/\n&/g"        | # NL BEFORE <g
            sed "s/<\/g>/&\n/g"     | # NL AFTER  </g>
            sed '/^<g/!s/</\n&/g'   | # NL BEFORE < IF NO <g
                  sed '/^[ \t]*$/d'                   | # DELETE EMPTY LINES
                  sed 's/>[ \t]*</></g'               | # SQUEEZE BLANKS 1
                  tr -s ' '                           | # SQUEEZE BLANKS 2
                  grep -n ""`                           # NUMBER LINES
   do
    # ------------------------------------------------------------------- #
      printf . # INCREASE STATUS
    # ------------------------------------------------------------------- #
      if   [ `echo $ELEMENT | grep "zpos=\"[0-9]"  | wc -l` -gt 0 ]
       then   ZPOS=`echo $ELEMENT | sed "s/zpos=/\nzpos/g" | \
                    grep "^zpos" | cut -d "\"" -f 2`"000"

              printf -- "$ZPOS " >> ${TMP}.zpos
              if [ `grep -s -- "^${ZPOS} " ${TMP}.zpos | #
                    wc -l` -gt 0 ];then
              CONTINUEZPOS=`grep -- "^${ZPOS} " \
                                         ${TMP}.zpos   | # FIND ZPOS
                            tail -n 2                  | # SELECT LAST 2 (PREV+NOW)
                            head -n 1                  | # SELECT PREV
                            sed 's/[ ]$//'             | # RM SPACES AT THE END
                            rev | cut -d " " -f 1 | rev` # SELECT LAST FIELD
              fi
              ZPOS=`expr $CONTINUEZPOS + 1`
              printf -- "$ZPOS " >> ${TMP}.zpos

              ELEMENT=`echo $ELEMENT | #
                       sed "s/ zpos=\"[0-9]\+\"/ zpos=\"${ZPOS}\"/g" | #
                       sed "s/lzpos/zpos/g"`
              ZPOS=`echo $ELEMENT | sed "s/zpos=/\nzpos/g" | \
                    grep "^zpos" | cut -d "\"" -f 2`

              echo ${ZPOS}:$ELEMENT          >> ${TMP}.2.tmp
      elif [ `echo $ELEMENT | grep "zpos=\"-[0-9]" | wc -l` -gt 0 ]
       then   ZPOS=`echo $ELEMENT | sed "s/zpos=/\nzpos/g" | \
                    grep "^zpos" | cut -d "\"" -f 2`"000"

              printf -- "$ZPOS " >> ${TMP}.zpos
              if [ `grep -s -- "^${ZPOS} " ${TMP}.zpos | #
                    wc -l` -gt 0 ];then
              CONTINUEZPOS=`grep -- "^${ZPOS} " \
                                         ${TMP}.zpos   | # FIND ZPOS
                            tail -n 2                  | # SELECT LAST 2 (PREV+NOW)
                            head -n 1                  | # SELECT PREV
                            sed 's/[ ]$//'             | # RM SPACES AT THE END
                            rev | cut -d " " -f 1 | rev` # SELECT LAST FIELD
              fi
              ZPOS=`expr $CONTINUEZPOS + 1`
              printf -- "$ZPOS " >> ${TMP}.zpos

              ELEMENT=`echo $ELEMENT | #
                       sed "s/ zpos=\"-[0-9]\+\"/ zpos=\"${ZPOS}\"/g" | #
                       sed "s/lzpos/zpos/g"`
              ZPOS=`echo $ELEMENT | sed "s/zpos=/\nzpos/g" | \
                    grep "^zpos" | cut -d "\"" -f 2`
              echo ${ZPOS}:$ELEMENT          >> ${TMP}.0.tmp
      else    echo $ELEMENT | grep -v "EREF" >> ${TMP}.1.tmp
      fi;     echo $ELEMENT | grep "EREF"    >> ${TMP}.eref.tmp
              echo >> ${TMP}.zpos # LINEBREAK
    # ------------------------------------------------------------------- #
  done; )

  NLFOO=Nn${RANDOM}lL
  L=`basename $SVGOUT | sed 's/\.svg$//'`

  sed ':a;N;$!ba;s/\n/::NL::/g' $UNLAYERED | # RM ALL NEWLINES
  sed 's,</*svg[^>]*>,\n&\n,g'             | # SEPARATE <svg>
  sed 's,</*defs[^>]*>,\n&\n,g'            | # SEPARATE <defs>
  egrep '^<svg|^</*defs'                   | # SELECT svg/defs TAGS
  sed 's/::NL::/\n/g'                      > $UNLAYEREDSORTED

  if [ -f $TEMPLATE ] &&
     [ "$TEMPLATE" != "" ]
  then

   sed ':a;N;$!ba;s/\n//g' $TEMPLATE      | # REMOVE ALL LINEBREAKS
   sed 's/<g/\n&/g'                       | # MOVE GROUP TO NEW LINES
   sed '/groupmode="layer"/s/<g/4Fgt7R/g' | # PLACEHOLDER FOR LAYERGROUP OPEN
   sed ':a;N;$!ba;s/\n/ /g'               | # REMOVE ALL LINEBREAKS
   sed 's/4Fgt7R/\n<g/g'                  | # RESTORE LAYERGROUP OPEN + NEWLINE
   sed 's/<\/svg>//g'                     | # RM CLOSE TAG
   sed "s/^[ \t]*//"                      | # REMOVE LEADING BLANKS
   tee ${TMP}.head.tmp                    | # SEPARATE TEMPLATE HEAD
   grep "^<g"                             | # GROUPS (=LAYERS) ONLY
   tr -s ' ' >> ${TMP}.template.tmp         # SQUEEZE SPACES (+APPEND TO FILE)

   cat ${TMP}.head.tmp                    | # SHOW HEAD
   sed  's,<sodipodi:namedview,\n&,'      | # SEPARATE
   sed  's,</sodipodi:namedview>,&\n,'    | # SEPARATE
   grep '^<sodipodi:namedview'            | # SELECT
   tr -s ' ' >> $UNLAYEREDSORTED            # SQUEEZE SPACES (+APPEND TO FILE)

   cat ${TMP}.template.tmp >> $UNLAYEREDSORTED

  fi

  if [ "$ADDREF" != "" ];then
  echo "<g inkscape:groupmode=\"layer\""            >> $UNLAYEREDSORTED
  echo "   id=\"ref\" style=\"display:none\""       >> $UNLAYEREDSORTED
  echo "   sodipodi:insensitive=\"true\""           >> $UNLAYEREDSORTED
  echo "   inkscape:label=\"XX_CANVASREFS\">"       >> $UNLAYEREDSORTED
  cat ${TMP}.eref.tmp 2> /dev/null | #
  cut -d ":" -f 2- | sed '/^[ ]*$/d'                >> $UNLAYEREDSORTED
  echo -e "</g>"                                    >> $UNLAYEREDSORTED
  fi

  echo "<g inkscape:groupmode=\"layer\""            >> $UNLAYEREDSORTED
  echo "   sodipodi:insensitive=\"true\""           >> $UNLAYEREDSORTED
  echo "   id=\"$L\" inkscape:label=\"$L\">"        >> $UNLAYEREDSORTED
  cat ${TMP}.0.tmp 2> /dev/null                     | #
  sort -t : -t : -k1,1n -k2,2n                      | # SORT NUM FIELDS
  cut -d ":" -f 3- | sed '/^[ ]*$/d'                >> $UNLAYEREDSORTED
  cat ${TMP}.1.tmp 2> /dev/null | #
  sort -t : -t : -k1,1n -k2,2n                      | # SORT NUM FIELDS
  cut -d ":" -f 2- | sed '/^[ ]*$/d'                >> $UNLAYEREDSORTED
  cat ${TMP}.2.tmp 2> /dev/null                     | #
  sort -t : -t : -k1,1n -k2,2n                      | # SORT NUM FIELDS
  cut -d ":" -f 3- | sed '/^[ ]*$/d'                >> $UNLAYEREDSORTED
  echo -e "</g>\n</svg>"                            >> $UNLAYEREDSORTED

  mv $UNLAYEREDSORTED $SVGOUT
  sed -i "1s|^.*$|&\n$THISCMD\n$SRCSTAMP|" $SVGOUT

# =========================================================================== #
# CLEAN UP AND EXIT
# =========================================================================== #
  if [ `echo ${TMP} | wc -c` -ge 4 ] &&
     [ `ls ${TMP}*.* 2>/dev/null | wc -l` -gt 0 ];then rm ${TMP}*.* ;fi
# --------------------------------------------------------------------------- #
  echo " CIAOCIAO."

exit 0;
