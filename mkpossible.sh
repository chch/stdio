#!/bin/bash

# MAKE POSSIBLE LAYOUT FOR FURTHER EXAMINATION                                #
# --------------------------------------------------------------------------- #
# Copyright (c) 2020 Christoph Haag                                           #
# --------------------------------------------------------------------------- #

# This is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
# 
# The software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License below for more details.
# 
# -> http://www.gnu.org/licenses/gpl.txt

# --------------------------------------------------------------------------- #
# INCLUDE FUNCTIONS
# --------------------------------------------------------------------------- #
  source lib/sh/select.functions
# --------------------------------------------------------------------------- #
# PROVIDED ARGUMENTS
# --------------------------------------------------------------------------- #
  RXC=`echo $* | sed 's/ /\n/g' | grep "^[0-9]\+x[0-9]\+$"`
  if [ ! -z $RXC ]; then  ROWS=`echo $RXC | cut -d "x" -f 1`
                          COLS=`echo $RXC | cut -d "x" -f 2`
                    else  ROWS=10;COLS=7; fi
  XDENSITY=`echo $* | sed 's/ /\n/g' | grep "^[0-9]*$" | head -n 1`
  YDENSITY=`echo $* | sed 's/ /\n/g' | grep "^[0-9]*$" | tail -n 1`
  if [ -z $XDENSITY ];then XDENSITY="RRRRR";YDENSITY="RRRRR";fi
  INPUTLISTS=`echo $* | sed 's/ /\n/g' | grep -v "^-" | grep "\.list$"`
  for THIS in $INPUTLISTS
   do L=`echo $THIS | sed 's/^[0-9]\+x//'` #
      N=`echo $THIS                      | #
         sed 's/^\([0-9]\+\)\(.*\)/\1/'  | #
         egrep '^[0-9]+$'`                 #
      if [ -f "$L" ]
       then if [ "$N" != "" ]
             then L=`seq $N | sed "s|^.*$|$L|"`
            fi
            LALL=`echo $LALL $L`
      fi
  done; INPUTLISTS="$LALL"
  EXCEPTIONS=`echo $* | sed 's/-except/\n&/g' | sed 's/ /\n/g' | #
              grep "^-except" | cut -d "=" -f 2-`
# --------------------------------------------------------------------------- #
# STATIC DEFINITIONS
# --------------------------------------------------------------------------- #
  OUTDIR="_";LSDIR="E";SRCDIR="src/ioio";TMP="/tmp/STDIO${RANDOM}";
 #----
  ELEMENTLIST="${TMP}.all";if [ -f $ELEMENTLIST ];then rm $ELEMENTLIST; fi
   if [ `ls $INPUTLISTS | wc -l` -ge 1 ] &&
      [ `echo $INPUTLISTS | wc -c` -ge 2 ] 
    then # echo "LISTS PROVIDED"
         for L in `echo $INPUTLISTS`
          do LID=`echo $(($RANDOM + 50000 * 4)) | #
                  rev | cut -c 1-6`
             cat $L   | #
             sed "s/^/${LID}|/" >> $ELEMENTLIST
         done
    else # echo "NO LISTS PROVIDED"
         cat $LSDIR/*.list  | # TAKE ALL LISTS
         sort -u            | # REMOVE DUPLICATES
         sed "s/^/000000|/" | # DUMMY L(IST)ID 
         tee  >  $ELEMENTLIST # WRITE TO FILE
   fi;   cp $ELEMENTLIST ${ELEMENTLIST}.standard
 #----
  HTML=${OUTDIR}/dump.html
  IORRAY=${OUTDIR}/`date +%y%m%d%H%M%S`.ios
  echo "// $RXC" > $IORRAY
 #----
  for XCPTN in `echo $EXCEPTIONS | sed 's/;/\n/g'`
    do  L=`echo $XCPTN | cut -d ":" -f 2`
        echo $XCPTN | sed "s|,|:$L\n|g" >> ${TMP}.tmp
  done; touch ${TMP}.tmp # MAKE SURE FILE EXISTS ANYWAY

  cat ${TMP}.tmp                    | # USELESS USE OF CAT
  sed 's/\b[RC]\+[0-9]\+/\n&/g'     | #
  sed 's/:$//' | sed '/^[ ]*$/d'    | #
  awk 'BEGIN { FS = ":" } ; \
      { print length($1) ":" $0; }' | #
  sort -nr | cut -d ":" -f 2-       | #
  tee  > ${TMP}.exceptions            # WRITE TO FILE
 #----
# --------------------------------------------------------------------------- #
# START HTML
# --------------------------------------------------------------------------- #
  echo '<html><head>'    >  $HTML                         
  function w() { echo $* >> $HTML ; }
  w "<!-- $0 $* ("`date +%d.%m.%Y" "%H:%M:%S`") -->"
  w '<meta charset="utf-8"/>'
  w '<link rel="stylesheet" type="text/css" media="all" href="../lib/html/ios.css">'
  w '<script src="../lib/js/jquery.js" type="text/javascript"></script>'
  w "<script src=\"../$IORRAY\" type="text/javascript"></script>"
  w '<script src="../lib/js/cycliste.js" type="text/javascript"></script>'
  w '<script src="../lib/js/dump.js" type="text/javascript"></script>'
  w '</head><body>'
  w '<div class="master">'
# --------------------------------------------------------------------------- #
# MAKE IT!
# --------------------------------------------------------------------------- #
  Y=1;
  while [ $Y -le $ROWS ];
   do  X=1
    while [ $X -le $COLS ]
     do
        XP=`echo 00$X | rev | cut -c 1-2 | rev`
        YP=`echo 00$Y | rev | cut -c 1-2 | rev`

        printf "R${YP}C${XP}"
      # ------------------------------------------------------- #
        RND=$((RANDOM%10))
        if   [ $XDENSITY == RRRRR ];then RNDX=".."
        elif [ $RND -gt $XDENSITY ];then RNDX="00"
        elif [ $RND -le $XDENSITY ];then RNDX="[A-Z][A-Z]";fi
        RND=$((RANDOM%10))
        if   [ $YDENSITY == RRRRR ];then RNDY=".."
        elif [ $RND -gt $YDENSITY ];then RNDY="00";
        elif [ $RND -le $YDENSITY ];then RNDY="[A-Z][A-Z]";fi
      # ------------------------------------------------------- #
      # CHECK IF EXCEPTIONAL
      # ------------------------------------------------------- #
        XCPTN=`egrep "^R${YP}C${XP}:|^R${YP}:|^C${XP}:" \
               ${TMP}.exceptions | head -n 1`
        if [ "$XCPTN" != "" ]
        then XLST=`echo $XCPTN | cut -d ":" -f 2`
             if [ `ls $XLST | #
                   wc -l` -gt 0 ]
             then echo -e "  \e[104m\e[97m USE EXCEPTION \e[0m"
                  printf "      ";
                  LID=`echo $(($RANDOM + 50000 * 4)) | #
                       rev | cut -c 1-6`
                  cat $XLST | #
                  sed "s/^/${LID}|/" > $ELEMENTLIST
                  RNDX="..";RNDY=".."
              fi
        else      cp ${ELEMENTLIST}.standard $ELEMENTLIST
        fi
      # ------------------------------------------------------- #
        F1=`echo $MEMOF3  | sed 's/;/\n/g' | #
            grep "^R$(($Y - 1))C${X}:" | cut -d ":" -f 2`
        if   [ $Y -eq 1 ];     then  F1=00 ; F3=$RNDY
        elif [ $Y -eq $ROWS ]; then  F3=00;
                               else  F3=$RNDY ;fi
        if   [ $X -eq 1 ];     then  F2=$RNDX ; F4=00
        elif [ $X -eq $COLS ]; then  F4=$F2   ; F2=00
                               else  F4=$F2   ; F2=$RNDX  ;fi

          printf "   NEEDED: _${F1}_${F2}_${F3}_${F4}_\n"
          selectElement
          printf   "         SELECT: _${F1}_${F2}_${F3}_${F4}_\n"
          selectElement

          ID="id=\"R${YP}C${XP}\""
          CLASS="class=\"r${YP} c${XP}"
 
        # WRITE ARRAY
        # -----------
          NL=N${RANDOM}L
          echo $SELECTMORE | sed "s/ /\n/g"      | # PRINT RESULTS AS LINES
          sed 's/\(.*\)\(:\)\(.*$\)/\3\1\2\3/'   | # COPY TRNSFRM TO FRONT
          sed 's/^ROTO/0/' | sed 's/^FL[IO]P/1/' | # NUMERIZE TRNSFRM
          sed 's/^.\{15\}/&|/'                   | # SEPARATE INFO (TRNSFRM/NAME)
          sort -u -t "|" -k 1,1                  | # SORT/UNIQ ACCORDING TO INFO
          sed 's/^[0-9]\{4\}//'                  | # RM NUMERIZED TRNSFRM
          sed 's/|//'                            | # UNDO INFO SEPARATION
          shuf                                   | # RANDOMIZE
          sed ":a;N;\$!ba;s/\n/$NL/g"            | # SEPARATE LINES
          sed "s/^/R${YP}C${XP} = ['/"           | # START ARRAY
          sed "s/$/'];/"                         | # CLOSE ARRAY
          sed "s/$NL/',\n          '/g"          | # INDENT
          tee >> $IORRAY                           # WRITE TO LIST

        # WRITE HTML
        # ---------- 
          grep "R${YP}C${XP}" $IORRAY | head -n 1                         | #
          sed "s,^.* = \[',<div $ID style=\"background-image:url(,"        | #
          sed "s,\([0-9]\{3\}\)'\]*;*\,*$,)\" CXLXAXSXS a\1\"></div>,"      | #
          sed "s/:\([FLIRTOP]\{4\}\))\" CXLXAXSXS/)\" $CLASS k \1/"          | #
          sed 's/ ROTO / /' | sed 's/ FLIP / flip /' | sed 's/ FLOP / flop /' | #
          tee >> $HTML                                  # WRITE TO FILE

          ISSINGLE=`grep "^R${YP}C${XP}.*];$" $IORRAY | wc -l`

          if [ "$ISSINGLE" -lt 1 ]
           then
                echo "<div $CLASS b cycliste\"> \
                      <a href=\"#R${YP}C${XP}\"> \
                      </a></div>" | #
                sed 's/>[ ]*</></g' | tr -s ' '       | # SQUEEZE SPACES
                tee >> $HTML                            # WRITE TO FILE
          fi

          SVGSRC=`grep "R${YP}C${XP}" $IORRAY | #
                  head -n 1 | cut -d "'" -f 2 | #
                  cut -d ":" -f 1`              #
          echo "<div $CLASS b svgsrc\"> \
                <a href=\"$SVGSRC\" class=\"svgsrc\" \
                 id=\""R${YP}C${XP}SRC"\"></a></div>" | #
          sed 's/>[ ]*</></g' | tr -s ' '             | # SQUEEZE SPACES
          tee >> $HTML                                  # WRITE TO FILE
  
        # DO REMEMBER
        # -----------
          MEMOF3="${MEMOF3};R${Y}C${X}:${F3}";
          X=`expr $X + 1`

    done
    Y=`expr $Y + 1`
  done
# --------------------------------------------------------------------------- #
# CLOSE HTML
# --------------------------------------------------------------------------- #
  w '</div>'
  w '<button id="create">DUMP</button>'
  w "<a href=\"#\" id=\"togglesrc\" class=\"ts\">SVGSRC</a>"
  w '</body></html>'
# --------------------------------------------------------------------------- #
# CLEAN UP
# --------------------------------------------------------------------------- #
  if [ `echo ${TMP} | wc -c` -ge 4 ] &&
     [ `ls ${TMP}*.* 2>/dev/null | wc -l` -gt 0 ]
  then  rm ${TMP}*.* ;fi

exit 0;
