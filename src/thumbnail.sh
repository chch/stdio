#!/bin/bash

 SVGDIR="ioio";TMP="/tmp/STDIOTHMB"
# =========================================================================== #
# CHECK EXIFTOOL
# --------------------------------------------------------------------------- #
  if [ `hash exiftool 2>&1 | wc -l` -gt 0 ]
  then EXIF="OFF";echo "PLEASE INSTALL exiftool";exit 0;else EXIF="ON"; fi
# =========================================================================== #
# DO IT NOW!
# =========================================================================== #
  for SVG in `ls $SVGDIR | sed "s/^/$SVGDIR\//" | grep "\.svg$"`
   do
     OUT=$SVGDIR/`basename $SVG | sed 's/\.svg$//'`.gif
   # --------------------------------------------------------------------- #
     if   [ ! -f $OUT ] || [ $SVG -nt $OUT ];then
   # --------------------------------------------------------------------- #
     echo "WRITING: $OUT"

     cp $SVG ${TMP}.svg    
     inkscape --vacuum-defs ${TMP}.svg

     BG='<path style="fill:#ccffaa;" \
          d="m 0\,0 800\,0 0\,800 -800\,0 z" id="c" />'
   # CANVAS='<path style="fill:#ffffff;" \
   #          d="m 0\,0 400\,0 0\,400 -400\,0 z" id="c" />'
     TRANSFORM='transform="scale(0.5\,0.5) translate(600\,600)"'        

     sed -i "s,</metadata>,&$BG<g $TRANSFORM>$CANVAS,g" ${TMP}.svg
     sed -i 's,</svg>,</g>&,g'                          ${TMP}.svg
     sed -i 's/stroke-width:[0-9\.]*/stroke-width:2/g'  ${TMP}.svg
     sed -i 's/height="400"/height="800"/g'             ${TMP}.svg
     sed -i 's/width="400"/width="800"/g'               ${TMP}.svg
     sed -i 's/fill:#000000/fill:#555555/g'             ${TMP}.svg

     COUNT=0
     for COLOR in `cat ${TMP}.svg | sed 's/style="/\n&/g' | #
                   grep "^style" | sed 's/#/\n#/g' | #
                   grep '^#' | cut -c 1-7 | #
                   sed -re '/#[0-9A-Fa-f]{6}/!d' | #
                   sort -u | grep -v "#ffffff"`
      do
         cp ${TMP}.svg ${TMP}2.svg
         sed -i -re "s/$COLOR/XxXxXx/g"           ${TMP}2.svg  # PROTECT COLOR
         sed -i -re 's/#[0-9A-Fa-f]{6}/#ffffff/g' ${TMP}2.svg  # ALL HEX TO WHITE
         sed -i -re "s/XxXxXx/#000000/g"          ${TMP}2.svg  # COLOR TO BLACK

         inkscape --export-pdf=${TMP}.pdf ${TMP}2.svg
         convert -monochrome -flatten ${TMP}.pdf ${TMP}.gif

         convert ${TMP}.gif -fill $COLOR -opaque black ${COUNT}.gif
         if [ $COUNT -gt 0 ]; then
         composite -compose Multiply -gravity center \
                    collect.gif ${COUNT}.gif ${TMP}.gif
              mv ${TMP}.gif collect.gif
              rm ${COUNT}.gif
         else
              mv ${COUNT}.gif collect.gif
              rm ${TMP}.gif
         fi
         COUNT=`expr $COUNT + 1`
     done

     convert collect.gif -transparent "#ccffaa" $OUT
   # --------------------------------------------------------------------- #
     SRCSTAMP=`grep '^<!-- .*\.svg ([0-9a-f]\+/[0-9a-f]\+) -->$' $SVG | #
               sed 's/^<!--[ ]*//' | sed 's/[ ]*-->$//'`
     if [ "$EXIF" == ON ]; then
           exiftool -Source="$SRCSTAMP" $OUT > /dev/null 2>&1
           if [ -f ${OUT}_original ];then rm ${OUT}_original; fi
     fi
   # --------------------------------------------------------------------- #
     rm ${TMP}.svg ${TMP}2.svg ${TMP}.pdf collect.gif 
   # --------------------------------------------------------------------- #
     else echo "THUMB UP-TO-DATE: $OUT"
   # --------------------------------------------------------------------- #
     fi
  done
# =========================================================================== #

exit 0;
