#!/bin/bash

# --------------------------------------------------------------------------- #
# MAKE IOIOs LOCAL
# --------------------------------------------------------------------------- #
  TMP="tmp";SP="///---\\\|||" # https://unix.stackexchange.com/q/225179
# ---
  IOIOLOKAL="ioio"
  IOIOREMOTEMASTER="https://freeze.sh/_/ioio/_"
# --------------------------------------------------------------------------- #
# GET FLAGS
# --------------------------------------------------------------------------- #
  IOIOREMOTEOTHERS=`echo $* | sed 's/ /\n/g'            | #
                    egrep '^--from=|^--remote=|^--src=' | #
                    cut -d '=' -f 2-                    | #
                    sed 's/[,|]/::N::/g'                | #
                    sed 's/\/$//'`                        #
  IOIOREMOTES="${IOIOREMOTEMASTER}::N::${IOIOREMOTEOTHERS}"
# --
  if [ `echo $* | sed 's/ /\n/g' | grep -- "^--update" | wc -l` -gt 0 ]
  then MODUS="UPDATE"; echo "MODUS: $MODUS"
  fi
# --
  IOIOSRC=`echo $*                       | #
           sed 's/ /\n/g'                | #
           grep -v "^-"                  | #
           egrep "\.list$|\.ios$|\.html$"` #
  if [ "$IOIOSRC" == "" ]
  then echo "PLEASE PROVIDE INPUT FILE"
       echo './lokalize.sh ../_/*.html 2> missing.log'
       exit 1;fi
# --------------------------------------------------------------------------- #
# COLLECT IOIO NAMES
# --------------------------------------------------------------------------- #
  for EACH in `echo $*`
   do echo -en " COLLECT IOIO NAMES \r"
      printf "\b${SP:i++%${#SP}:1}"
      if [ `echo $EACH | grep "\.html$" | wc -l` -gt 0 ]
      then  cat $EACH `cat $EACH | sed 's/"/\n/g' | grep '\.ios$'`| #
            sed 's/[A-F0-9]\{,12\}.....[CUSI0WB_]\{13\}/\n&\n/g'  | #
            egrep '^[A-F0-9]{,12}.....[CUSI0WB_]{13}$' >> ${TMP}.tmp

      elif [ `echo $EACH | egrep "\.ios$|\.list$" | wc -l` -gt 0 ]
      then    sed 's/[A-F0-9]\{,12\}.....[CUSI0WB_]\{13\}/\n&\n/g' $EACH | #
              egrep '^[A-F0-9]{,12}.....[CUSI0WB_]{13}$' >> ${TMP}.tmp
      fi
  done
# --------------------------------------------------------------------------- #
  NUM=`cat ${TMP}.tmp | sort -u | wc -l`
# --------------------------------------------------------------------------- #
  for IOIONAME in `cat ${TMP}.tmp | sort -u`
   do     # ----
      IOIO="$IOIOLOKAL/${IOIONAME}.svg"
      if [ ! -f "$IOIO" ]
      then  DONE="";
            for IOIOREMOTE in `echo $IOIOREMOTES | #
                               sed 's/::N::/\n/g'` #
             do RMTE="$IOIOREMOTE/${IOIONAME}.svg"
                if [ "$DONE" != "DONE" ]
                then  HTTPSTATUS=`curl -s -o /dev/null -IL \
                                       -w "%{http_code}" "$RMTE"`
               # --
                 if [ "$HTTPSTATUS" == "200" ]
                 then C="\e[93m"
                      curl -sRL "$RMTE" -o $IOIO
                      DONE="DONE"
                 fi
               # --
                fi
            done
            if [ "$DONE" != "DONE" ]
            then C="\e[31m"
                 >&2 echo "MISSING: $IOIONAME"
                 sleep 0.5
            fi
       else C="\e[92m"
       # ------------------------------------------------------------- #
         if [ "$MODUS" == "UPDATE" ];then
       # ------------------------------------------------------------- #
         DONE="";
         for IOIOREMOTE in `echo $IOIOREMOTES | #
                            sed 's/::N::/\n/g'` #
          do RMTE="$IOIOREMOTE/${IOIONAME}.svg"
             if [ "$DONE" == "" ]
             then HTTPSTATUS=`curl -s -o /dev/null -IL \
                                   -w "%{http_code}" "$RMTE"`
            # --
              if [ "$HTTPSTATUS" == "200" ]
              then DONE="$RMTE";else RMTE=""
              fi
            # --
             fi
         done
         RMTE="$DONE"
       # ------------------------------------------------------------- #
         if [ "$RMTE" != "" ];then
       # ------------------------------------------------------------- #
       # COMPARE FILESIZE
       # ------------------------------------------------------------- #
         LBYTES=`stat --printf="%s" $IOIO`
         RBYTES=`curl -sI $RMTE  | grep '^Content-Length:' | #
                 cut -d ':' -f 2 | sed 's/[^0-9]//g'`        #
       
       # IF FILESIZE IS DIFFERENT -> DOWNLOAD
       # ------------------------------------------------------------- #
         if [ "$LBYTES" != "$RBYTES" ]
         then C="\e[93m"
              curl -sRL "$RMTE" -o $IOIO
       # IF FILESIZE IS EQUAL -> MAKE BYTECHECK
       # ------------------------------------------------------------- #
         else BRNGE="100";BFRST="80"
              BLAST=`expr $BFRST + $BRNGE`
       
           RBYTECHECK=`curl -s -r ${BFRST}-${BLAST} $RMTE    | #
                       md5sum`
           BRNGE=`expr $BRNGE + 1`;BLAST=`expr $BFRST + $BRNGE`
           LBYTECHECK=`head -c $BLAST $IOIO | tail -c $BRNGE | #
                       md5sum`
           # ----------------------------------------------------- #
           # IF BYTECHECK IS DIFFERENT -> DOWNLOAD
           # ----------------------------------------------------- #
             if [ "$LBYTECHECK" != "$RBYTECHECK" ]
             then  C="\e[93m" 
                   curl -sRL "$RMTE" -o $IOIO
             else  C="\e[92m"
             fi
           # ----------------------------------------------------- #
       # ------------------------------------------------------------- #
         fi
       # ------------------------------------------------------------- #
         else C="\e[31m"
              if [ ! -f "$IOIO" ] 
              then >&2 echo "MISSING: $IOIONAME"
              else >&2 echo "NO REMOTE FOR $IOIONAME"
              fi
         fi
       # ------------------------------------------------------------- #
         fi
       # ------------------------------------------------------------- #
      fi
      printf "\b${SP:i++%${#SP}:1}"
      echo -en " $C$IOIONAME   \e[39m$NUM STILL TO DO              \r"
      NUM=`expr $NUM - 1`
  done
# --------------------------------------------------------------------------- #
  echo -en "\r"
  echo "CIAOCIAO.                                                            "
# --------------------------------------------------------------------------- #
# CLEAN UP
# --------------------------------------------------------------------------- #
  if [ `echo ${TMP} | wc -c` -ge 4 ] &&
     [ `ls ${TMP}*.* 2>/dev/null | wc -l` -gt 0 ]
  then  rm ${TMP}*.* ;fi;echo -e "\n"

exit 0;
