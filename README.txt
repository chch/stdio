 A DAY AT THE STDIO
 ------------------

 Read more soon ...............




 -------------------------------------------------------------------
 COPYRIGHT (C) 2016 Christoph Haag
 -------------------------------------------------------------------
 If not stated otherwise permission is granted to copy, distribute
 and/or modify these documents under the  terms of the Creative 
 Commmons Attribution-ShareAlike 4.0 International License 

 -> http://creativecommons.org/licenses/by-sa/4.0/

 -------------------------------------------------------------------
 EXCEPT: lib/* *.sh
 -------------------------------------------------------------------
 COPYRIGHT (C) 2016 Christoph Haag
 -------------------------------------------------------------------
 These files are free software: you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation, either version 3 of
 the License, or (at your option) any later version.

 They are distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty
 of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License below for more details.

 -> http://www.gnu.org/licenses/gpl.txt

 -------------------------------------------------------------------

 Your fair use and other rights are not affected by the above.

 TRADEMARKS, QUOTED MATERIAL AND LINKED CONTENT
 IS COPYRIGHTED ACCORDING TO ITS RESPECTIVE OWNERS.

 E N J O Y !


